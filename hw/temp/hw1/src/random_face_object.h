#ifndef RANDOM_FO_H
#define RANDOM_FO_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct RandomFaceObject;
struct RandomFaceObjectArgs;

void init_RandomFaceObject(struct Object*,
                           struct RandomFaceObjectArgs);
void new_RandomFaceObject(struct Object**,
                          struct RandomFaceObjectArgs);
void RandomFaceObject_render(struct Object*, Camera*);
void RandomFaceObject_free(struct Object*);
void RandomFaceObject_recolor(struct RandomFaceObject*);
void RandomFaceObject_update(Object*, SceneState*);

struct RandomFaceObjectArgs {
  GLchar* obj_file_path;
  ConfigurableShader* shader;
} typedef RandomFaceObjectArgs;

enum RFO_RENDER_MODE {
                      RFO_FACE_AND_WIREFRAME=1,
                      RFO_FACE_ONLY=2,
                      RFO_WIREFRAME_ONLY=3,
                      RFO_POINTS_ONLY=4,
};

struct RandomFaceObject {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  GLuint render_mode;
  ConfigurableShader* shader;
} typedef RandomFaceObject;

#endif /* RANDOM_FO_H */
