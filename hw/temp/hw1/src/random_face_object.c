#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <time.h>

#include "config_shader.h"
#include "random_face_object.h"
#include "object.h"
#include "object_reader.h"
#include "util.h"
#include "camera.h"

#define WIDTH 1280
#define HEIGHT 800

void init_RandomFaceObject(Object* container,
                           RandomFaceObjectArgs args) {
  RandomFaceObject *self = malloc(sizeof(RandomFaceObject));
  Mesh obj;
  parse_obj(args.obj_file_path, &obj);

  GLuint vertices_size = obj.number_of_faces*18*sizeof(GLfloat);
  GLfloat* vertices = malloc(vertices_size);
  Face* cur = obj.head;
  int vpos = 0;
  while (cur != NULL) {
    vec3 color;
    gen_rnd_color(color);

    vertices[vpos]    = cur->face[0][0];
    vertices[vpos+1]  = cur->face[0][1];
    vertices[vpos+2]  = cur->face[0][2];
    vertices[vpos+3]  = color[0];
    vertices[vpos+4] = color[1];
    vertices[vpos+5] = color[2];
    vertices[vpos+6]  = cur->face[1][0];
    vertices[vpos+7]  = cur->face[1][1];
    vertices[vpos+8]  = cur->face[1][2];
    vertices[vpos+9]  = color[0];
    vertices[vpos+10] = color[1];
    vertices[vpos+11] = color[2];
    vertices[vpos+12]  = cur->face[2][0];
    vertices[vpos+13]  = cur->face[2][1];
    vertices[vpos+14]  = cur->face[2][2];
    vertices[vpos+15]  = color[0];
    vertices[vpos+16] = color[1];
    vertices[vpos+17] = color[2];

    vpos += 18;
    cur = cur->next;
  }

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, vertices_size,
               vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        6*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        6*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0); /* unbind vao */

  self->number_of_faces = obj.number_of_faces;
  self->render_mode = RFO_FACE_ONLY;
  self->shader = args.shader;

  IODesc obj_desc = {
    .free_addr=RandomFaceObject_free,
    .render_addr=RandomFaceObject_render,
    .update_addr=RandomFaceObject_update
  };

  init_Object(container, (void*) self, obj_desc);

  printf("Object:\n");
  printf("-NumFaces %d\n", self->number_of_faces);

  // NOTE positions object in scene
  glm_rotate(self->shader->model, glm_rad(45.0), (vec3){1.0, 0.0, 0.0});

  obj.free(&obj);
  free(vertices);
}

void new_RandomFaceObject(Object** container,
                          RandomFaceObjectArgs args) {
  (*container) = malloc(sizeof(Object));
  init_RandomFaceObject((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {\
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE 0.1f
#define ROT_STEP_SIZE 10.0f
void RandomFaceObject_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  RandomFaceObject *our_obj = (RandomFaceObject*) container->object;

  UPDATE_CASE(is, key_1)
      our_obj->render_mode = RFO_FACE_ONLY;
  UPDATE_CASE(is, key_2)
      our_obj->render_mode = RFO_WIREFRAME_ONLY;
  UPDATE_CASE(is, key_3)
      our_obj->render_mode = RFO_POINTS_ONLY;
  UPDATE_CASE(is, key_4)
      our_obj->render_mode = RFO_FACE_AND_WIREFRAME;
  UPDATE_CASE(is, key_0)
    DEBOUNCE(RandomFaceObject_recolor(our_obj), RECOLOR);
  UPDATE_CASE(is, key_w) /* forward (away) */
      glm_translate(our_obj->shader->model,
                    (vec3){0.0f, 0.0f, -1*STEP_SIZE});
  UPDATE_CASE(is, key_s) /* backwards (towards) */
      glm_translate(our_obj->shader->model, (vec3){0.0f, 0.0f, STEP_SIZE});
  UPDATE_CASE(is, key_a) /* left */
      glm_translate(our_obj->shader->model,
                    (vec3){-1*STEP_SIZE, 0.0f, 0.0f});
  UPDATE_CASE(is, key_d) /* right */
      glm_translate(our_obj->shader->model, (vec3){STEP_SIZE, 0.0f, 0.0f});
  UPDATE_CASE(is, key_e) /* down */
      glm_translate(our_obj->shader->model,
                    (vec3){0.0f, -1*STEP_SIZE, 0.0f});
  UPDATE_CASE(is, key_q) /* up */
      glm_translate(our_obj->shader->model, (vec3){0.0f, STEP_SIZE, 0.0f});
  UPDATE_CASE(is, key_u) /* rotate x ccw */
      glm_rotate(our_obj->shader->model,
                glm_rad(ROT_STEP_SIZE), (vec3){1.0, 0.0, 0.0});
  UPDATE_CASE(is, key_i) /* rotate x cw */
      glm_rotate(our_obj->shader->model,
                 glm_rad(-1*ROT_STEP_SIZE), (vec3){1.0, 0.0, 0.0});
  UPDATE_CASE(is, key_j) /* rotate y ccw */
      glm_rotate(our_obj->shader->model,
                 glm_rad(ROT_STEP_SIZE), (vec3){0.0, 1.0, 0.0});
  UPDATE_CASE(is, key_k) /* rotate y cw */
      glm_rotate(our_obj->shader->model,
                 glm_rad(-1*ROT_STEP_SIZE), (vec3){0.0, 1.0, 0.0});
  UPDATE_CASE(is, key_o) /* rotate z ccw */
      glm_rotate(our_obj->shader->model,
                 glm_rad(ROT_STEP_SIZE), (vec3){0.0, 0.0, 1.0});
  UPDATE_CASE(is, key_p) /* rotate z cw */
      glm_rotate(our_obj->shader->model,
                 glm_rad(-1*ROT_STEP_SIZE), (vec3){0.0, 0.0, 1.0});
}

void RandomFaceObject_recolor(RandomFaceObject* self) {
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  vec3 color;
  for (int i = 0; i < 3*self->number_of_faces; i++) {
    if (i % 3 == 0)
      gen_rnd_color(color);

    glBufferSubData(GL_ARRAY_BUFFER,
                    (GLintptr)(i*6*sizeof(GLfloat) + 3*sizeof(GLfloat)),
                    3*sizeof(GLfloat), color);
  }
}

void RandomFaceObject_render(Object* container, Camera* camera) {
  RandomFaceObject *self = (RandomFaceObject*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);
  self->shader->wireframe = false;
  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  switch (self->render_mode) {
  case RFO_FACE_ONLY:
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    break;
  case RFO_FACE_AND_WIREFRAME:
    self->shader->wireframe = false;
    self->shader->use(self->shader);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDrawArrays(GL_TRIANGLES, 0, 16*self->number_of_faces);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    self->shader->wireframe = true;
    self->shader->use(self->shader);
    break;
  case RFO_POINTS_ONLY:
    glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    break;
  case RFO_WIREFRAME_ONLY:
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    break;
  }
  glDrawArrays(GL_TRIANGLES, 0, 16*self->number_of_faces);
  glBindVertexArray(0);
}

void RandomFaceObject_free(Object* container) {
  RandomFaceObject *self = (RandomFaceObject*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
