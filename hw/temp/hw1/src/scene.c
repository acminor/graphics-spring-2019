#include "scene.h"
#include "input.h"
#include "random_face_object.h"
#include "object.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  init_InputState(&(self->state.input_state));
  self->objects.number_of_objects = 0;

  glm_mat4_identity(self->camera.view);
  glm_mat4_identity(self->camera.projection);
  glm_translate(self->camera.view, (vec3){0.0f,0.0f,-4.0f});
  glm_perspective(glm_rad(45.0f), (GLfloat)WIDTH/(GLfloat)HEIGHT,
                  0.1f, 100.0f, self->camera.projection);

  // TODO should we add shader stuff to rfo???
	// Build and compile our shader program
  {
    Shader* our_shader;
    new_Shader(&our_shader, "res/main.vert.glsl", "res/main.frag.glsl");
    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    RandomFaceObjectArgs args = {
    obj_file_path:"res/eight.uniform.obj",
    shader:our_conf_shader
    };
    int obj_num = self->objects.number_of_objects;
    init_RandomFaceObject(&(self->objects.objs[obj_num]), args);
    self->objects.number_of_objects++;
  }

  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

void Scene_update(Scene* self, SceneState state) {
  self->state = state;
  int num_objs = self->objects.number_of_objects;
  for (int i = 0; i < num_objs; i++) {
    Object *obj = &self->objects.objs[i];
    obj->update(obj, &self->state);
  }
}

void Scene_render(Scene* self) {
  int num_objs = self->objects.number_of_objects;
  for (int i = 0; i < num_objs; i++) {
    Object *obj = &self->objects.objs[i];
    obj->render(obj, &self->camera);
  }
}
