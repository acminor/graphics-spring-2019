#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

void mn_read_file(GLchar** buff, FILE* file);
void gen_rnd_color(vec3 color);

#endif /* UTIL_H */
