#+TITLE: Homework 4 Build Document
#+AUTHOR: Austin Minor (2018280284)

* Purpose
This document serves a brief description of the process and layout
of the code along with how to build and run the code. The incremental
approach of Homework 1, 2 and 3 were helpful in building this code.

* Building the Code
The code is built using Autotools. This was chosen as it is ported to
a wide range of platforms and integrates well with my chosen build
environment. Their may be issues if you are using a non-Linux/Unix computer
to build the following code. The simplest solution in this case is to use
a POSIX compatible environment for your platform. One example of this is
to use CYGWIN on Windows.

Assuming your build environment is correct, you may need to install some
external library dependencies. This is not covered here; however, information
for such external libraries can surely be found online. Assuming everything
is installed properly, there are two simple steps to building the code.

1) In a terminal/console run "./configure".
2) If everything succeeded, run "make".

After the program has compiled, you must run it from the root directory of
the project with the following command "./src/program". It must be ran
from the root to have proper access to external resource files.

NOTE: The code has not been tested on Windows, but it does compile
      on Fedora Linux 64-bit.

* Code History and Layout
  
The main code in this project continues similarly to the objects defined
in previous homework assignments. We mainly draw inspiration and code
from the Homework 3 assignment. This code is concerned with the rendering
of text. We continued to use the TA's code for text rendering because it
was a good base for creating code that could render moving sprites as
it was essentially code for rendering displaced text.

Currently our source is in the "src" folder and libraries that we depend
on are in the "libs" folder. The source layout is as follows. "main.c"
handles our engine initialize, main loop, and input. From here, our object
is handled by a scene "scene.h/c". This scene responds to input and passes
it to objects "object.h/c". These objects are generic and allow us to
have many individual types of objects with a common interface.
The main thing in this code is that we learned from Homework 2 to create
an emitter object for the particles. This is handled by the "snow.c/h"
files. We also believe this code to be unique as it allows for the
not only randomization in size but also randomization in fall direction
and speed.

* Program Mappings
What follows if a table of mappings on how to control our program.

|-----+-----------------------------|
| Key | Action                      |
|-----+-----------------------------|
| w   | Move forward                |
| s   | Move backward               |
| a   | Move left                   |
| d   | Move right                  |
| e   | Move down                   |
| q   | Move up                     |
|-----+-----------------------------|
