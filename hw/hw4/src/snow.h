#ifndef SNOW_H
#define SNOW_H

#include <glad/glad.h>

#include "planet_reader.h"
#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Snow;
struct SnowArgs;
struct SnowParticle;

void init_Snow(struct Object*,
                 struct SnowArgs);
void new_Snow(struct Object**,
                struct SnowArgs);
void Snow_render(struct Object*, Camera*);
void Snow_free(struct Object*);
void Snow_update(Object*, SceneState*);
void Snow_new_particle(struct Snow*);

struct SnowArgs {
  ConfigurableShader* shader;
  GLuint particle_lifetime;
  GLuint particle_creation_time;
  GLfloat size_max;
  GLfloat size_min;
} typedef SnowArgs;

struct SnowParticle {
  GLuint time;
  GLfloat dir_theta;
  GLfloat dir_velocity;
  GLfloat size;
  GLfloat spos_x;
  GLfloat spos_y;
} typedef SnowParticle;

struct Snow {
  GLuint vao;
  GLuint vbo;
  ConfigurableShader* shader;
  Array particles;
  GLuint time;
  GLuint particle_creation_time;
  GLuint particle_lifetime;
  GLuint bg_tex;
  GLuint snow_tex;
  GLfloat size_max;
  GLfloat size_min;
} typedef Snow;

#endif /* SNOW_H */
