#ifndef OBJECT_H
#define OBJECT_H

#include <stdlib.h>
#include "camera.h"

struct SceneState;
struct Object {
  void* object;
  void (*free)(struct Object*);
  void (*render)(struct Object*, Camera*);
  void (*update)(struct Object*, struct SceneState*);
} typedef Object;

struct IODesc { // NOTE internal object descriptor
  void (*free_addr)(struct Object*);
  void (*render_addr)(struct Object*, Camera*);
  void (*update_addr)(struct Object*, struct SceneState*);
} typedef IODesc;

void init_Object(Object* obj, void* io, IODesc io_desc);

#endif /* OBJECT_H */
