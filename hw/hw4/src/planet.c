#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "config_shader.h"
#include "planet.h"
#include "object.h"
#include "object_reader.h"
#include "planet_reader.h"
#include "util.h"
#include "camera.h"
#include "text.h"

#define WIDTH 1280
#define HEIGHT 800

void init_Planet(Object* container,
                           PlanetArgs args) {
  Planet *self = malloc(sizeof(Planet));
  Mesh obj;
  parse_obj(args.obj_file_path, &obj);

  GLuint vertices_size = obj.number_of_faces*9*sizeof(GLfloat);
  GLfloat* vertices = malloc(vertices_size);
  Face* cur = obj.head;
  int vpos = 0;
  while (cur != NULL) {
    // NOTE: we will use texture mapping lat-long provided
    // here https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Textured_Spheres
    vertices[vpos]    = cur->face[0][0];
    vertices[vpos+1]  = cur->face[0][1];
    vertices[vpos+2]  = cur->face[0][2];
    vertices[vpos+3]  = cur->face[1][0];
    vertices[vpos+4]  = cur->face[1][1];
    vertices[vpos+5]  = cur->face[1][2];
    vertices[vpos+6]  = cur->face[2][0];
    vertices[vpos+7]  = cur->face[2][1];
    vertices[vpos+8]  = cur->face[2][2];

    vpos += 9;
    cur = cur->next;
  }

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, vertices_size,
               vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  self->number_of_faces = obj.number_of_faces;
  self->shader = args.shader;
  self->data = args.data;
  self->current_frame = 0;
  self->speed_scale = 0.006;

  int width, height;
  unsigned char* image =
    SOIL_load_image(args.data->texture, &width, &height,
                    0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->shader->texture);
  glBindTexture(GL_TEXTURE_2D, self->shader->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);

  {
    Shader* shader;
    new_Shader(&shader, "res/text.vert.glsl", "res/text.frag.glsl");
    ConfigurableShader *conf_shader;
    new_ConfigurableShader(&conf_shader, shader);

    Object* text;
    new_Text(&text, (TextArgs){
          .shader=conf_shader,
          .text=self->data->name,
          .x=0.0f,
          .y=0.0f,
          .scale=0.2f});
    self->text = text;
  }

  IODesc obj_desc = {
    .free_addr=Planet_free,
    .render_addr=Planet_render,
    .update_addr=Planet_update
  };

  init_Object(container, (void*) self, obj_desc);

  printf("Object:\n");
  printf("-NumFaces %d\n", self->number_of_faces);

  // NOTE positions object in scene
  glm_rotate(self->shader->model, glm_rad(45.0), (vec3){1.0, 0.0, 0.0});

  obj.free(&obj);
  free(vertices);
}

void new_Planet(Object** container,
                          PlanetArgs args) {
  (*container) = malloc(sizeof(Object));
  init_Planet((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {                \
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Planet_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  Planet *self = (Planet*) container->object;

  float theta = self->speed_scale*self->current_frame;
  self->current_frame++;
  float eccentricity =
    (self->data->rmax - self->data->rmin) /
    (self->data->rmax + self->data->rmin);
  float sc = 23481.066;
  float semi_latus_rectum =
    .006*sc*self->data->rmin * (1 + eccentricity);

  float radius = semi_latus_rectum / (1 + eccentricity*cos(theta));
  float x = radius*cos(theta);
  float y = radius*sin(theta);
  /*
  printf("Planet %s, x: %f y: %f\n", self->data->name, x, y);
  printf("Planet %s, rmax: %f rmin: %f\n", self->data->name,
         sc*self->data->rmax, sc*self->data->rmin);
  printf("Planet %s, radius: %f p-radius: %f\n",
         self->data->name,
         radius, self->data->earth_radius);
  */

  if (abs(self->data->rmin - 1.382) < .001) {
    //    printf("Planet %s, x: %f y: %f\n", self->data->name, x, y);
    //    printf("Found Mars\n");
  }

  float scale = self->data->earth_radius;
  mat4 temp;
  mat4 scalem, translate;
  glm_mat4_identity(temp);

  if (self->data->rmin < .0001) {
    // sun
    scale /= 8;
    glm_scale(temp, (vec3){scale, scale, scale});
    glm_translate_to(temp,
                     (vec3){0.0f, 0.0f, 0.0f},
                     self->shader->model);
  } else {
    scale *= 4;
    glm_scale_make(scalem, (vec3){scale,scale,scale});
    glm_translate_make(translate, (vec3){x,y,0.0f});
    glm_mat4_mulN((mat4 *[]){&translate, &scalem}, 2, self->shader->model);
  }

  Text *text = (Text*) self->text->object;
  text->x = x;
  text->y = y + scale + 10;
  self->text->update(self->text, state);

  UPDATE_CASE(is, key_2)
    self->speed_scale*=1.01;
  UPDATE_CASE(is, key_1)
    self->speed_scale/=1.01;
}

void Planet_render(Object* container, Camera* camera) {
  Planet *self = (Planet*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);
  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  glDrawArrays(GL_TRIANGLES, 0, 9*self->number_of_faces);
  glBindVertexArray(0);

  self->text->render(self->text, camera);
}

void Planet_free(Object* container) {
  Planet *self = (Planet*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);

  free(self->data->texture);
  free(self->data->name);
  free(self->data);
}
