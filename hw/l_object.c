#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <time.h>
#include <macro_collections.h>

#include "array.h"
#include "config_shader.h"
#include "l_object.h"
#include "object.h"
#include "object_reader.h"
#include "util.h"
#include "camera.h"

#define WIDTH 1280
#define HEIGHT 800

LIST_GENERATE(list, list, , GLfloat*)

int vec3_pred(GLfloat* org, GLfloat* elem) {
  volatile bool res = ((fabs(org[0] - elem[0]) < .0001) &&
                       (fabs(org[1] - elem[1]) < .0001) &&
                       (fabs(org[2] - elem[2]) < .0001));
  vec3 diff;
  glm_vec3_sub(org, elem, diff);
  float d = glm_vec3_dot(diff,diff);

  if (res) {
    return 0;
  }

  return 1;
}

void init_LObject(Object* container,
                           LObjectArgs args) {
  LObject *self = (LObject*) malloc(sizeof(LObject));
  Mesh obj;
  parse_obj(args.obj_file_path, &obj);

  list *verts = list_new(100);
  vec3 *f_norms = (vec3 *) malloc(obj.number_of_faces*sizeof(vec3));
  Face* cur = obj.head;
  int fpos = 0;
  while (cur != NULL) {
    vec3 p1, p2, p3;
    glm_vec3_copy(cur->face[0], p1);
    glm_vec3_copy(cur->face[1], p2);
    glm_vec3_copy(cur->face[2], p3);

    if (list_indexof(verts, p1, vec3_pred, true) == verts->count) {
      GLfloat* temp = (GLfloat*) malloc(3*sizeof(GLfloat));
      glm_vec3_copy(p1, temp);
      list_push_back(verts, temp);
    }
    if (list_indexof(verts, p2, vec3_pred, true) == verts->count) {
      GLfloat* temp = (GLfloat*) malloc(3*sizeof(GLfloat));
      glm_vec3_copy(p2, temp);
      list_push_back(verts, temp);
    }
    if (list_indexof(verts, p3, vec3_pred, true) == verts->count) {
      GLfloat* temp = (GLfloat*) malloc(3*sizeof(GLfloat));
      glm_vec3_copy(p3, temp);
      list_push_back(verts, temp);
    }

    cur = cur->next;
    /*
    vec3 v1, v2;
    glm_vec3_sub(p2, p3, v1);
    glm_vec3_sub(p1, p3, v2);
    vec3 normal;
    glm_cross(v2, v1, f_norms[fpos]);
    fpos++;
    */
  }

  vec3 *v_norms = (vec3 *) malloc(verts->count*sizeof(vec3));
  for (int i = 0; i < verts->count; i++) {
    vec3 zeros = {0.0f,0.0f,0.0f};
    glm_vec3_copy(zeros, v_norms[i]);
  }

  /*
  fpos = 0;
  cur = obj.head;
  while (cur != NULL) {
    for (int i = 0; i < 3; i++) {
      int vi = list_indexof(verts, cur->face[i], vec3_pred, true);
      glm_vec3_add(f_norms[fpos], v_norms[vi], v_norms[vi]);
    }

    fpos++;
    cur = cur->next;
  }
  */

  cur = obj.head;
  GLuint vertices_size = obj.number_of_faces*18*sizeof(GLfloat);
  GLfloat* vertices = (GLfloat*) malloc(vertices_size);
  list *elements = list_new(100);
  int vpos = 0;
  while (cur != NULL) {
    for (int i = 0; i < 3; i++)
      list_push_back(elements, cur->face[i]);

    /*
    for (int i = 0; i < 3; i++) {
      vec3 norm;
      int vi = list_indexof(verts, cur->face[i], vec3_pred, true);
      glm_vec3_copy(v_norms[vi], norm);
      for (int j = 0; j < 3; j++) {
        vertices[vpos+6*i+j] = cur->face[i][j];
      }
      for (int j = 0; j < 3; j++) {
        vertices[vpos+6*i+j+3] = norm[j];
      }
    }
    vpos += 18;
    */

    /*
    vertices[vpos]    = cur->face[0][0];
    vertices[vpos+1]  = cur->face[0][1];
    vertices[vpos+2]  = cur->face[0][2];
    vertices[vpos+3]  = cur->face[1][0];
    vertices[vpos+4]  = cur->face[1][1];
    vertices[vpos+5]  = cur->face[1][2];
    vertices[vpos+6]  = cur->face[2][0];
    vertices[vpos+7]  = cur->face[2][1];
    vertices[vpos+8]  = cur->face[2][2];
    vpos += 9;
    */

    cur = cur->next;
  }

  list* normaltemp = list_new(100);
  cur = obj.head;
  for (int i = 0; i < elements->count; i++) {
    if ((i+1)%3 == 0) {
      vec3 a, b;
      glm_vec3_sub(list_get(elements,i-1), list_get(elements,i), a);
      glm_vec3_sub(list_get(elements,i-2), list_get(elements,i), b);
      GLfloat* res = malloc(3*sizeof(GLfloat));
      glm_vec3_cross(b,a,res);
      list_push_back(normaltemp, res);
    }
  }

  for (int i = 0; i < elements->count; i++) {
    int vi = list_indexof(verts, list_get(elements,i), vec3_pred, true);
    glm_vec3_add(v_norms[vi], list_get(normaltemp,1/3), v_norms[vi]);
  }

  cur = obj.head;
  while (cur != NULL) {
      for (int i = 0; i < 3; i++) {
        vec3 norm;
        int vi = list_indexof(verts, cur->face[i], vec3_pred, true);
        glm_vec3_copy(v_norms[vi], norm);
        for (int j = 0; j < 3; j++) {
          vertices[vpos+6*i+j] = cur->face[i][j];
        }
        for (int j = 0; j < 3; j++) {
          vertices[vpos+6*i+j+3] = norm[j];
        }
      }
      vpos += 18;
      cur = cur->next;
  }

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, vertices_size,
               vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        6*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE,
                        6*sizeof(GLfloat), (GLvoid*)(3*sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0); /* unbind vao */

  self->number_of_faces = obj.number_of_faces;
  self->shader = args.shader;
  self->light = args.light;
  self->material = args.material;

  IODesc obj_desc = {
    .free_addr=LObject_free,
    .render_addr=LObject_render,
    .update_addr=LObject_update
  };

  init_Object(container, (void*) self, obj_desc);

  printf("Object:\n");
  printf("-NumFaces %d\n", self->number_of_faces);

  // NOTE positions object in scene
  vec3 axis = {1.0,0.0,0.0};
  glm_rotate(self->shader->model, glm_rad(45.0), axis);

  obj.free(&obj);
  free(vertices);
}

void new_LObject(Object** container,
                          LObjectArgs args) {
  (*container) = (Object*) malloc(sizeof(Object));
  init_LObject((*container), args);
}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE 0.1f
#define ROT_STEP_SIZE 10.0f
void LObject_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  LObject *our_obj = (LObject*) container->object;
}

void LObject_render(Object* container, Camera* camera) {
  LObject *self = (LObject*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);

  GLint cPosLoc = glGetUniformLocation(self->shader->shader->program,
                                       "viewPos");
  glUniform3fv(cPosLoc, 1, camera->position);

  Material_set_uniforms(self->material, self->shader->shader);
  Light_set_uniforms(self->light, self->shader->shader);

  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  glDrawArrays(GL_TRIANGLES, 0, 6*self->number_of_faces);
  glBindVertexArray(0);
}

void LObject_free(Object* container) {
  LObject *self = (LObject*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
