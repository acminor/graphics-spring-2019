#ifndef PLANET_H
#define PLANET_H

#include <glad/glad.h>

#include "planet_reader.h"
#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Planet;
struct PlanetArgs;

void init_Planet(struct Object*,
                           struct PlanetArgs);
void new_Planet(struct Object**,
                          struct PlanetArgs);
void Planet_render(struct Object*, Camera*);
void Planet_free(struct Object*);
void Planet_update(Object*, SceneState*);

struct PlanetArgs {
  GLchar* obj_file_path;
  ConfigurableShader* shader;
  PlanetData* data;
} typedef PlanetArgs;

struct Planet {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  ConfigurableShader* shader;
  PlanetData* data;
  unsigned int current_frame;
  GLfloat speed_scale;
  Object* text;
} typedef Planet;

#endif /* PLANET_H */
