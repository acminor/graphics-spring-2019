#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "scene.h"
#include "input.h"
#include "beizer_surface.h"
#include "array.h"
#include "object.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  init_InputState(&(self->state.input_state));
  self->objects.number_of_objects = 0;

  glm_mat4_identity(self->camera.view);
  glm_mat4_identity(self->camera.projection);
  //glm_rotate(self->camera.view, .3, (vec3){0.0f,0.0f,1.0f});
  glm_translate(self->camera.view, (vec3){0.0f,0.0f,-8.0f});
  self->camera.pos[0] = 0.0f;
  self->camera.pos[1] = 0.0f;
  self->camera.pos[2] = -4.0f;
  glm_perspective(glm_rad(45.0f), (GLfloat)WIDTH/(GLfloat)HEIGHT,
                  0.1f, 1000.0f, self->camera.projection);

  {
    Shader* our_shader;
    new_TessShader(&our_shader,
                    "res/bz.vert.glsl",
                    "res/bz.frag.glsl",
                    "res/bz.tcs.glsl",
                    "res/bz.tes.glsl");

    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    BeizerSurfaceArgs args = {
                              .shader=our_conf_shader,
    };
    init_BeizerSurface(&(self->objects.objs[0]), args);
  }

  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE .4
void Scene_update(Scene* self, SceneState state) {
  self->state = state;
  InputState is = self->state.input_state;

  Object *obj = &self->objects.objs[0];
  obj->update(obj, &self->state);
}

void Scene_render(Scene* self) {
  Object *obj = &self->objects.objs[0];
  obj->render(obj, &self->camera);
}
