#ifndef CONFIG_SHADER_H
#define CONFIG_SHADER_H

#include <cglm/cglm.h>
#include <stdbool.h>

#include "shader.h"

struct ConfigurableShader {
  Shader* shader;
  mat4 model;
  mat4 view;
  mat4 projection;
  int texture;
  bool wireframe;
  bool points;
  bool faces;
  float uOuter02;
  float uOuter13;
  float uInner0;
  float uInner1;
  void (*use)(struct ConfigurableShader*);
} typedef ConfigurableShader;

void ConfigurableShader_use(ConfigurableShader*);
void init_ConfigurableShader(ConfigurableShader*, Shader*);
void new_ConfigurableShader(ConfigurableShader**, Shader*);

#endif /* CONFIG_SHADER_H */
