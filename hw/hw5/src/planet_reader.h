#ifndef PLANET_READER_H
#define PLANET_READER_H

#include <stdio.h>
#include <string.h>
#include "array.h"
#include "util.h"

struct PlanetData {
  char* name;
  char* texture;
  float rmin;
  float rmax;
  float earth_radius;
} typedef PlanetData;

void pr_read_planet_data(char* data_path, Array** data);

#endif /* PLANET_READER_H */
