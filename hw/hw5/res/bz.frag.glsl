#version 330 core

in vec2 TexCoord;
uniform bool wireframe_color;
out vec4 color;

uniform sampler2D ourTexture;

void main()
{
  if (wireframe_color) {
    color = vec4(0.4, 0.4, 0.4, 1.0);
  } else {
    color = texture(ourTexture, TexCoord);
  }
}
