#version 330 core

in vec3 pos;
uniform sampler2D tex;

void main()
{
  vec2 longLat = vec2((atan(pos.y, pos.x) / 3.14 + 1.0) * 0.5,
                      (asin(pos.z) / 3.14 + 0.5));

  gl_FragColor = texture2D(tex, longLat);
}
