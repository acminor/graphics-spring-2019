#version 410 core

layout(quads, equal_spacing, ccw) in;

out vec2 TexCoord;

float bernstein_poly_eval(int i, int n, float u);
float fact(int i);

void main() {
	float u = gl_TessCoord.x;
  float v = gl_TessCoord.y;
  vec4 pos = vec4(0.,0.,0.,0.);
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      float bi_5 = bernstein_poly_eval(i, 5, u);
      float bj_5 = bernstein_poly_eval(j, 5, v);
      pos += bi_5*bj_5*gl_in[i+j*5].gl_Position;
    }
  }

  TexCoord = vec2(u, v);
  gl_Position = pos;
}

float bernstein_poly_eval(int i, int n, float u) {
  float num = fact(n)*pow(u, i)*pow(1.0f-u, n-i);
  float denom = fact(i)*fact(n-i);
  return num/denom;
}

float fact(int i) {
  float val = 1.0f;
  for (; i > 0; i--) {
    val *= i;
  }
  return val;
}
