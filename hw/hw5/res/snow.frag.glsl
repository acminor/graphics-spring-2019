#version 330 core
in vec2 TexCoords;
out vec4 color;

uniform sampler2D ourTexture;
uniform vec3 textColor;

void main()
{
  color = texture(ourTexture, TexCoords);
}
