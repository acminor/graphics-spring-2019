#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec3 color;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform bool wireframe_color;

out vec3 ourColor;
out vec3 ourPos;

void main()
{
    gl_Position = projection * view * model * vec4(position, 1.0f);
    ourPos = position;

    if (!wireframe_color) {
      ourColor = color;
    } else {
      ourColor = vec3(0.0f, 0.0f, 0.0f);
    }
}
