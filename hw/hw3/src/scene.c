#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "scene.h"
#include "input.h"
#include "planet.h"
#include "planet_reader.h"
#include "array.h"
#include "object.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  init_InputState(&(self->state.input_state));
  self->objects.number_of_objects = 0;

  glm_mat4_identity(self->camera.view);
  glm_mat4_identity(self->camera.projection);
  //glm_rotate(self->camera.view, .3, (vec3){0.0f,0.0f,1.0f});
  glm_translate(self->camera.view, (vec3){0.0f,0.0f,-400.0f});
  self->camera.pos[0] = 0.0f;
  self->camera.pos[1] = 0.0f;
  self->camera.pos[2] = -400.0f;
  glm_perspective(glm_rad(45.0f), (GLfloat)WIDTH/(GLfloat)HEIGHT,
                  0.1f, 100000.0f, self->camera.projection);

  {
    Shader* our_shader;
    new_Shader(&our_shader, "res/planet.vert.glsl", "res/planet.frag.glsl");

    Array* planets;
    pr_read_planet_data("res/planets.dat", &planets);

    for (int i = 0; i < planets->length; i++) {
      ConfigurableShader *our_conf_shader;
      new_ConfigurableShader(&our_conf_shader, our_shader);

      PlanetData* data;
      Array_get(planets, i, (void**) &data);
      PlanetArgs args = {
                         .obj_file_path="res/ball.obj",
                         .shader=our_conf_shader,
                         .data=data
      };

      int obj_num = self->objects.number_of_objects;
      init_Planet(&(self->objects.objs[obj_num]), args);
      self->objects.number_of_objects++;
    }
  }

  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE 4
void Scene_update(Scene* self, SceneState state) {
  self->state = state;
  InputState is = self->state.input_state;

  printf("Look at %f %f %f\n", is.look_at[0],
         is.look_at[1], is.look_at[2]);
  printf("Pos %f %f %f\n", self->camera.pos[0],
         self->camera.pos[1], self->camera.pos[2]);
  /*
  glm_translate(self->camera.view,
                (vec3){-self->camera.pos[0],
                         -self->camera.pos[1],
                         -self->camera.pos[2]});
  glm_look_anyup(self->camera.pos, is.look_at, self->camera.view);
  //glm_rotate(self->camera.view,
  //              .3, is.look_at);
  glm_translate(self->camera.view,
                self->camera.pos);
  */

  UPDATE_CASE(is, key_w) /* forward (away) */
    glm_translate(self->camera.view,
                  (vec3){0.0f, 0.0f, -1*STEP_SIZE});
  UPDATE_CASE(is, key_s) /* backwards (towards) */
    glm_translate(self->camera.view, (vec3){0.0f, 0.0f, STEP_SIZE});
  UPDATE_CASE(is, key_a) /* left */
    glm_translate(self->camera.view, (vec3){STEP_SIZE, 0.0f, 0.0f});
  UPDATE_CASE(is, key_d) /* right */
    glm_translate(self->camera.view,
                  (vec3){-1*STEP_SIZE, 0.0f, 0.0f});
  UPDATE_CASE(is, key_e) /* down */
    glm_translate(self->camera.view,
                  (vec3){0.0f, -1*STEP_SIZE, 0.0f});
  UPDATE_CASE(is, key_q) /* up */
    glm_translate(self->camera.view, (vec3){0.0f, STEP_SIZE, 0.0f});

  int num_objs = self->objects.number_of_objects;
  for (int i = 0; i < num_objs; i++) {
    Object *obj = &self->objects.objs[i];
    obj->update(obj, &self->state);
  }
}

void Scene_render(Scene* self) {
  int num_objs = self->objects.number_of_objects;
  for (int i = 0; i < num_objs; i++) {
      Object *obj = &self->objects.objs[i];
      obj->render(obj, &self->camera);
  }
}
