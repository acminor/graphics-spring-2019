#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <time.h>
#include <math.h>

#include "config_shader.h"
#include "text.h"
#include "object.h"
#include "object_reader.h"
#include "util.h"
#include "camera.h"

#define GLOBAL static
#define WIDTH 1280
#define HEIGHT 800

GLOBAL TextCharacter Text_CHARS[256];
GLOBAL bool Text_IS_INIT = false;

void init_Text(Object* container,
                           TextArgs args) {
  Text *self = malloc(sizeof(Text));
  // CITE: sprite matrix creates a square
  //       plane and properly maps to texture
  //       data. This set of vertices and
  //       coordinates comes from the offline
  //       version of the learnopengl.com tutorial
  //       for rendering sprites. You can download the
  //       pdf on their site. It is chapter 47.
  GLfloat sprite[] = {
   /*pos        tex*/
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f,
     0.0f, 0.0f, 0.0f, 0.0f,
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 1.0f, 1.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f
  };

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(sprite),
               sprite, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE,
                        4*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  float color[3];
  for (int i = 0; i < 3; i++)
    color[i] = 0xff;

  self->shader = args.shader;
  /*
  self->shader->color[0] = color[0];
  self->shader->color[1] = color[1];
  self->shader->color[2] = color[2];
  */
  self->q_display = true;
  self->text = args.text;
  self->scale = args.scale;
  self->x = args.x;
  self->y = args.y;

  if (!Text_IS_INIT) {
    FT_Library ft;
    if (FT_Init_FreeType(&ft))
      printf("ERROR::FREETYPE: Could not init FreeType Library\n");

    FT_Face face;
    if (FT_New_Face(ft, "res/arial.ttf", 0, &face))
      printf("ERROR::FREETYPE: Failed to load font\n");
    FT_Set_Pixel_Sizes(face, 0, 48);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    for (int c = 0; c < 256; c++) {
      if (FT_Load_Char(face, c, FT_LOAD_RENDER)) {
        printf("ERROR::FREETYPE: Failed to load Glyph\n");
        continue;
      }

      GLuint texture;
      glGenTextures(1, &texture);
      glBindTexture(GL_TEXTURE_2D, texture);
      glTexImage2D(
                   GL_TEXTURE_2D,
                   0,
                   GL_RED,
                   face->glyph->bitmap.width,
                   face->glyph->bitmap.rows,
                   0,
                   GL_RED,
                   GL_UNSIGNED_BYTE,
                   face->glyph->bitmap.buffer
                   );
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
      glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

      Text_CHARS[c].texId = texture;
      Text_CHARS[c].size.x = face->glyph->bitmap.width;
      Text_CHARS[c].size.y = face->glyph->bitmap.rows;
      Text_CHARS[c].bearing.x = face->glyph->bitmap_left;
      Text_CHARS[c].bearing.y = face->glyph->bitmap_top;
      Text_CHARS[c].advance = face->glyph->advance.x;
    }
    glBindTexture(GL_TEXTURE_2D, 0);
    FT_Done_Face(face);
    FT_Done_FreeType(ft);

    Text_IS_INIT = true;
  }

  IODesc obj_desc = {
    .free_addr=Text_free,
    .render_addr=Text_render,
    .update_addr=Text_update
  };

  init_Object(container, (void*) self, obj_desc);
}

void new_Text(Object** container,
                          TextArgs args) {
  (*container) = malloc(sizeof(Object));
  init_Text((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {\
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Text_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  Text *self = (Text*) container->object;

  UPDATE_CASE(is, key_3)
    self->q_display = false;
  UPDATE_CASE(is, key_4)
    self->q_display = true;
}

void Text_render(Object* container, Camera* camera) {
  Text *self = (Text*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);

  if (!self->q_display)
    return;

  int i = 0;
  char c = self->text[i];
  GLfloat x = self->x;
  GLfloat y = self->y;
  while (c != '\0') {
    TextCharacter ch = Text_CHARS[c];
    GLfloat scale = self->scale;

    GLfloat xpos = x + ch.bearing.x * scale;
    GLfloat ypos = y - (ch.size.y - ch.bearing.y) * scale;

    GLfloat w = ch.size.x * scale;
    GLfloat h = ch.size.y * scale;

    // Update VBO for each character
    GLfloat vertices[] =
      {
        xpos,     ypos + h,   0.0, 1.0,
        xpos,     ypos,       0.0, 0.0,
        xpos + w, ypos,       1.0, 0.0,
        xpos,     ypos + h,   0.0, 1.0,
        xpos + w, ypos,       1.0, 0.0,
        xpos + w, ypos + h,   1.0, 1.0
    };
    self->shader->texture = ch.texId;
    glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    self->shader->use(self->shader);
    glBindVertexArray(self->vao);
    glDrawArrays(GL_TRIANGLES, 0, 4*6);

    x += (ch.advance >> 6) * scale;

    i++;
    c = self->text[i];
  }

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void Text_free(Object* container) {
  Text *self = (Text*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
