#ifndef INPUT_H
#define INPUT_H

#include <cglm/cglm.h>

#define UNKNOWN_KEY_STATE -127
struct InputState {
  char key_1;
  char key_2;
  char key_3;
  char key_4;
  char key_0;
  char key_w;
  char key_s;
  char key_a;
  char key_d;
  char key_e;
  char key_q;
  char key_u;
  char key_i;
  char key_j;
  char key_k;
  char key_o;
  char key_p;
  vec3 look_at;
} typedef InputState;

void init_InputState(InputState *state);
void new_InputState(InputState** state);
void InputState_cursor_clear(InputState *state);

#endif /* INPUT_H */
