#ifndef TEXT_H
#define TEXT_H

#include<ft2build.h>
#include FT_FREETYPE_H

#include <glad/glad.h>
#include <stdbool.h>

#include "planet_reader.h"
#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Text;
struct TextArgs;

void init_Text(struct Object*,
                 struct TextArgs);
void new_Text(struct Object**,
                struct TextArgs);
void Text_render(struct Object*, Camera*);
void Text_free(struct Object*);
void Text_update(Object*, SceneState*);

struct TextPoint {
  int x;
  int y;
} typedef TextPoint;

struct TextCharacter {
  GLuint texId;
  TextPoint size;
  TextPoint bearing;
  GLuint advance;
} typedef TextCharacter;

struct TextArgs {
  ConfigurableShader* shader;
  char* text;
  GLfloat x;
  GLfloat y;
  GLfloat scale;
} typedef TextArgs;

struct Text {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  ConfigurableShader* shader;
  char* text;
  GLfloat x;
  GLfloat y;
  GLfloat scale;
  bool q_display;
} typedef Text;

#endif /* TEXT_H */
