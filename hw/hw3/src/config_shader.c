#include "config_shader.h"

void init_ConfigurableShader(ConfigurableShader* self, Shader* shader) {
  self->shader = shader;
  self->wireframe = false;
  self->points = false;
  self->faces = false;
  self->use = ConfigurableShader_use;
  self->texture = -1;

  glm_mat4_identity(self->model);
  glm_mat4_identity(self->view);
  glm_mat4_identity(self->projection);
}

void new_ConfigurableShader(ConfigurableShader** this, Shader* shader) {
  (*this) = malloc(sizeof(ConfigurableShader));
  init_ConfigurableShader((*this), shader);
}

void ConfigurableShader_use(ConfigurableShader* self) {
  self->shader->use(self->shader);

  GLint modelLoc = glGetUniformLocation(self->shader->program,
                                        "model");
  GLint viewLoc = glGetUniformLocation(self->shader->program,
                                       "view");
  GLint projLoc = glGetUniformLocation(self->shader->program,
                                       "projection");
  GLint wfColorLoc = glGetUniformLocation(self->shader->program,
                                          "wireframe_color");

  if (self->texture != -1)
    glBindTexture(GL_TEXTURE_2D, self->texture);

  glUniformMatrix4fv(modelLoc, 1, GL_FALSE, (GLfloat*) self->model);
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, (GLfloat*) self->view);
  glUniformMatrix4fv(projLoc, 1, GL_FALSE, (GLfloat*) self->projection);
  glUniform1i(wfColorLoc, self->wireframe);
}
