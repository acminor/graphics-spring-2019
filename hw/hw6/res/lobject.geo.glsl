#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 vFragPos[];
in mat3 normalMat[];

out vec3 FragPos;
out vec3 Normal;

void main(void)
{
  vec4 PositionIn[3] = vec4[]((gl_in[0].gl_Position),
                              (gl_in[1].gl_Position),
                              (gl_in[2].gl_Position));

  for(int i = 0; i < 3; i++) { // You used triangles, so it's always 3
    // reduced version of project2 stuff CITE
    int xm = (i+1) % 3;
    int ym = (i+2) % 3;
    int zm = i;
    Normal = normalize(cross(PositionIn[2].xyz - PositionIn[0].xyz,
                             PositionIn[1].xyz - PositionIn[0].xyz));
    /*
    Normal = normalize(cross(PositionIn[xm].xyz-PositionIn[zm].xyz,
                             PositionIn[ym].xyz-PositionIn[zm].xyz)).xyz;
    */
    Normal = normalMat[i] * Normal;
    FragPos = vFragPos[i];
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
  }
  EndPrimitive();
}
