#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "scene.h"
#include "input.h"
#include "l_object.h"
#include "array.h"
#include "object.h"
#include "light.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  init_InputState(&(self->state.input_state));
  self->objects.number_of_objects = 0;
  init_Camera(&self->camera);

  {
    Shader* our_shader;
    /*
    new_GeoShader(&our_shader,
                  "res/lobject.vert.glsl",
                  "res/lobject.frag.glsl",
                  "res/lobject.geo.glsl");
    */
    new_Shader(&our_shader,
               "res/lobject.vert.glsl",
               "res/lobject.frag.glsl");

    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    Material* material;
    new_Material(&material);
    Material_set_ambient(material, (vec3){1.0f,0.5f,0.31f});
    Material_set_diffuse(material, (vec3){1.0f,0.5f,0.31f});
    Material_set_specular(material, (vec3){0.5f,0.5f,0.5f});
    Material_set_shininess(material, 16.0f);

    Light* light;
    new_Light(&light);
    Light_set_ambient(light, (vec3){1.0f,0.5f,0.31f});
    Light_set_diffuse(light, (vec3){1.0f,0.5f,0.31f});
    Light_set_specular(light, (vec3){0.5f,0.5f,0.5f});
    Light_set_pos(light, (vec3){0.0f, 0.0f, 1.0f});

    LObjectArgs args = {
                        .shader=our_conf_shader,
                        .obj_file_path="res/eight.uniform.obj",
                        .material=material,
                        .light=light
    };
    init_LObject(&(self->objects.objs[0]), args);
  }

  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE .4
void Scene_update(Scene* self, SceneState state) {
  self->state = state;
  InputState is = self->state.input_state;

  Object *obj = &self->objects.objs[0];
  obj->update(obj, &self->state);

  Camera_update(&self->camera, state);
  printf("Camera pos (%f, %f, %f)\n",
         self->camera.position[0],
         self->camera.position[1],
         self->camera.position[2]);
  printf("Camera front (%f, %f, %f)\n",
         self->camera.front[0],
         self->camera.front[1],
         self->camera.front[2]);
}

void Scene_render(Scene* self) {
  Object *obj = &self->objects.objs[0];
  obj->render(obj, &self->camera);
}
