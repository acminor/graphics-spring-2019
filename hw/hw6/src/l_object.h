#ifndef L_O_H
#define L_O_H

#include <glad/glad.h>

#include "light.h"
#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct LObject;
struct LObjectArgs;

void init_LObject(struct Object*,
                           struct LObjectArgs);
void new_LObject(struct Object**,
                          struct LObjectArgs);
void LObject_render(struct Object*, Camera*);
void LObject_free(struct Object*);
void LObject_update(Object*, SceneState*);

struct LObjectArgs {
  GLchar* obj_file_path;
  ConfigurableShader* shader;
  Light* light;
  Material* material;
} typedef LObjectArgs;

struct LObject {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  ConfigurableShader* shader;
  Light* light;
  Material* material;
} typedef LObject;

#endif /* L_O_H */
