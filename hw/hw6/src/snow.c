#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "config_shader.h"
#include "object.h"
#include "util.h"
#include "camera.h"
#include "snow.h"

#define WIDTH 1280
#define HEIGHT 800

void init_Snow(Object* container,
                           SnowArgs args) {
  Snow *self = malloc(sizeof(Snow));
  // CITE: sprite matrix creates a square
  //       plane and properly maps to texture
  //       data. This set of vertices and
  //       coordinates comes from the offline
  //       version of the learnopengl.com tutorial
  //       for rendering sprites. You can download the
  //       pdf on their site. It is chapter 47.
  GLfloat sprite[] = {
   /*pos        tex*/
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f,
     0.0f, 0.0f, 0.0f, 0.0f,
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 1.0f, 1.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f
  };

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(sprite),
               sprite, GL_DYNAMIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE,
                        4*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  self->shader = args.shader;
  self->particle_lifetime = args.particle_lifetime;
  self->particle_creation_time = args.particle_creation_time;
  self->size_max = args.size_max;
  self->size_min = args.size_min;
  self->time = 0;
  Array_init(&self->particles);

  int width, height;
  unsigned char* image =
    SOIL_load_image("res/snow3.png", &width, &height,
                    0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->snow_tex);
  glBindTexture(GL_TEXTURE_2D, self->snow_tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);

  image =
    SOIL_load_image("res/background.png", &width, &height,
                    0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->bg_tex);
  glBindTexture(GL_TEXTURE_2D, self->bg_tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);

  Snow_new_particle(self);

  IODesc obj_desc = {
    .free_addr=Snow_free,
    .render_addr=Snow_render,
    .update_addr=Snow_update
  };

  init_Object(container, (void*) self, obj_desc);
}

void new_Snow(Object** container,
                          SnowArgs args) {
  (*container) = malloc(sizeof(Object));
  init_Snow((*container), args);
}

void _SnowParticle_freecb(void* particle) {
  SnowParticle *p = (SnowParticle*) particle;
  free(p);
}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Snow_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  Snow *self = (Snow*) container->object;

  if ((self->time % self->particle_creation_time) == 0) {
    Snow_new_particle(self);
  }

  for (int i = 0; i < self->particles.length; i++) {
    SnowParticle *p;
    Array_get(&self->particles, i, (void**) &p);
    if (p->time == self->particle_lifetime) {
      Array_remove(&self->particles, i, _SnowParticle_freecb);
      i--;
    }
  }

  self->time++;
}

void Snow_render(Object* container, Camera* camera) {
  Snow *self = (Snow*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);


  {
    GLfloat xpos = -300;
    GLfloat ypos = -200;
    GLfloat h = 400;
    GLfloat w = 600;
    // Update VBO for each character
    GLfloat vertices[] =
      {
       xpos,     ypos + h,   0.0, 1.0,
       xpos,     ypos,       0.0, 0.0,
       xpos + w, ypos,       1.0, 0.0,
       xpos,     ypos + h,   0.0, 1.0,
       xpos + w, ypos,       1.0, 0.0,
       xpos + w, ypos + h,   1.0, 1.0
      };
    glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    self->shader->texture = self->bg_tex;
    self->shader->use(self->shader);
    glBindVertexArray(self->vao);
    glDrawArrays(GL_TRIANGLES, 0, 4*6);
  }

  self->shader->texture = self->snow_tex;
  for (int i = 0; i < self->particles.length; i++) {
    SnowParticle* p;
    Array_get(&self->particles, i, (void**) &p);

    GLfloat x = p->spos_x + p->time*p->dir_velocity*cos(p->dir_theta);
    GLfloat y = p->spos_y + p->time*p->dir_velocity*sin(p->dir_theta);

    GLfloat xpos = x;
    GLfloat ypos = y;

    GLfloat w = p->size;
    GLfloat h = p->size;

    // Update VBO for each character
    GLfloat vertices[] =
      {
        xpos,     ypos + h,   0.0, 1.0,
        xpos,     ypos,       0.0, 0.0,
        xpos + w, ypos,       1.0, 0.0,
        xpos,     ypos + h,   0.0, 1.0,
        xpos + w, ypos,       1.0, 0.0,
        xpos + w, ypos + h,   1.0, 1.0
    };
    glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    self->shader->use(self->shader);
    glBindVertexArray(self->vao);
    glDrawArrays(GL_TRIANGLES, 0, 4*6);

    p->time++;
  }

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void Snow_free(Object* container) {
  Snow *self = (Snow*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}

void Snow_new_particle(Snow *self) {
  SnowParticle *particle = malloc(sizeof(SnowParticle));
  particle->time = 0;
  particle->dir_theta = glm_rad(250.0f + (rand()%40));
  particle->dir_velocity = (rand()%40 + 10.0f)/20.f;
  particle->size = ((rand()%40 + 20.0f)/40.f)*20;
  particle->spos_x = (rand()%400) - 200;
  particle->spos_y = 170;

  Array_append(&self->particles, (void*) particle);
}
