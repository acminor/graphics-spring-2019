#include <glad/glad.h>
#include <stdlib.h>
#include "light.h"

void init_Light(Light* this) {
  vec3 zeros = {0.0,0.0,0.0};
  glm_vec3_copy(zeros, this->pos);
  glm_vec3_copy(zeros, this->ambient);
  glm_vec3_copy(zeros, this->diffuse);
  glm_vec3_copy(zeros, this->specular);
}
void new_Light(Light** this) {
  (*this) = malloc(sizeof(Light));
  init_Light(*this);
}

void Light_set_ambient(Light* this, vec3 amb) {
  glm_vec3_copy(amb, this->ambient);
}

void Light_set_diffuse(Light* this, vec3 diff) {
  glm_vec3_copy(diff, this->diffuse);
}

void Light_set_specular(Light* this, vec3 spec) {
  glm_vec3_copy(spec, this->specular);
}

void Light_set_pos(Light* this, vec3 pos) {
  glm_vec3_copy(pos, this->pos);
}

void Light_set_uniforms(Light* this, Shader* sh) {
  GLint ambLoc = glGetUniformLocation(sh->program, "light.ambient");
  GLint diffLoc = glGetUniformLocation(sh->program, "light.diffuse");
  GLint specLoc = glGetUniformLocation(sh->program, "light.specular");
  GLint posLoc = glGetUniformLocation(sh->program, "light.pos");

  if (ambLoc == 0) {
    printf("Err");
    exit(0);
  }

  glUniform3fv(ambLoc, 1, this->ambient);
  glUniform3fv(diffLoc, 1, this->diffuse);
  glUniform3fv(specLoc, 1, this->specular);
  glUniform3fv(posLoc, 1, this->pos);
}

void init_Material(Material* this) {
  vec3 zeros = {0.0,0.0,0.0};
  glm_vec3_copy(zeros, this->ambient);
  glm_vec3_copy(zeros, this->diffuse);
  glm_vec3_copy(zeros, this->specular);
  this->shininess = 0.0f;
}
void new_Material(Material** this) {
  (*this) = malloc(sizeof(Material));
  init_Material(*this);
}

void Material_set_ambient(Material* this, vec3 amb) {
  glm_vec3_copy(amb, this->ambient);
}

void Material_set_diffuse(Material* this, vec3 diff) {
  glm_vec3_copy(diff, this->diffuse);
}

void Material_set_specular(Material* this, vec3 spec) {
  glm_vec3_copy(spec, this->specular);
}

void Material_set_shininess(Material* this, GLfloat shininess) {
  this->shininess = shininess;
}

void Material_set_uniforms(Material* this, Shader* sh) {
  GLint ambLoc = glGetUniformLocation(sh->program, "material.ambient");
  GLint diffLoc = glGetUniformLocation(sh->program, "material.diffuse");
  GLint specLoc = glGetUniformLocation(sh->program, "material.specular");
  GLint shLoc = glGetUniformLocation(sh->program, "material.shininess");

  glUniform3fv(ambLoc, 1, this->ambient);
  glUniform3fv(diffLoc, 1, this->diffuse);
  glUniform3fv(specLoc, 1, this->specular);
  glUniform1f(shLoc, this->shininess);
}
