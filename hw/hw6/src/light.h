#ifndef LIGHT_H
#define LIGHT_H

#include <cglm/cglm.h>
#include "shader.h"

struct Light {
  vec3 pos;
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
} typedef Light;

void init_Light(Light*);
void new_Light(Light**);

void Light_set_ambient(Light*, vec3);
void Light_set_diffuse(Light*, vec3);
void Light_set_specular(Light*, vec3);
void Light_set_pos(Light*, vec3);

void Light_set_uniforms(Light*, Shader*);

struct Material {
  GLfloat shininess;
  vec3 ambient;
  vec3 diffuse;
  vec3 specular;
} typedef Material;

void init_Material(Material*);
void new_Material(Material**);

void Material_set_ambient(Material*, vec3);
void Material_set_diffuse(Material*, vec3);
void Material_set_specular(Material*, vec3);
void Material_set_shininess(Material*, GLfloat);

void Material_set_uniforms(Material*, Shader*);

#endif /* LIGHT_H */
