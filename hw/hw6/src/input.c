#include <stdlib.h>
#include "input.h"

void init_InputState(InputState *state) {
  state->key_1 = UNKNOWN_KEY_STATE;
  state->key_2 = UNKNOWN_KEY_STATE;
  state->key_3 = UNKNOWN_KEY_STATE;
  state->key_4 = UNKNOWN_KEY_STATE;
  state->key_5 = UNKNOWN_KEY_STATE;
  state->key_6 = UNKNOWN_KEY_STATE;
  state->key_7 = UNKNOWN_KEY_STATE;
  state->key_8 = UNKNOWN_KEY_STATE;
  state->key_9 = UNKNOWN_KEY_STATE;
  state->key_0 = UNKNOWN_KEY_STATE;
  state->key_w = UNKNOWN_KEY_STATE;
  state->key_s = UNKNOWN_KEY_STATE;
  state->key_a = UNKNOWN_KEY_STATE;
  state->key_d = UNKNOWN_KEY_STATE;
  state->key_e = UNKNOWN_KEY_STATE;
  state->key_q = UNKNOWN_KEY_STATE;
  state->key_u = UNKNOWN_KEY_STATE;
  state->key_i = UNKNOWN_KEY_STATE;
  state->key_j = UNKNOWN_KEY_STATE;
  state->key_k = UNKNOWN_KEY_STATE;
  state->key_o = UNKNOWN_KEY_STATE;
  state->key_p = UNKNOWN_KEY_STATE;
  state->xoffset = 0.0f;
  state->yoffset = 0.0f;
  state->scroll_offset = 0.0f;
}

void new_InputState(InputState** state) {
  (*state) = malloc(sizeof(InputState));
  init_InputState(*state);
}
