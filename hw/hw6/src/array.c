#include <stdlib.h>
#include "array.h"
#include "object.h"

void Array_get(Array* arr, int i, void** elem) {
  if (i >= arr->length) {
    (*elem) = NULL;
  } else {
    (*elem) = arr->array[i];
  }
}

void Array_append(Array* arr, void* elem) {
  if (arr->length == arr->capacity) {
    arr->capacity *= 2;
    arr->array = realloc(arr->array, arr->capacity*sizeof(void*));
  }

  arr->array[arr->length] = elem;
  arr->length++;
}

void Array_remove(Array* arr, int i, void (*free_cb)(void*)) {
  if (i >= arr->length) {
    return;
  }

  free_cb(arr->array[i]);
  for (int j = i; j < arr->length-1; j++) {
    arr->array[j] = arr->array[j+1];
  }
  arr->length--;
}

void Array_clear(Array* arr, void (*free_cb)(void*)) {
  for (int i = 0; i < arr->length; i++) {
    free_cb(arr->array[i]);
  }

  arr->array = realloc(arr->array, ARRAY_INIT_SZ*sizeof(void*));
  arr->length = 0;
  arr->capacity = ARRAY_INIT_SZ;
}

void Array_init(Array* arr) {
  arr->array = calloc(ARRAY_INIT_SZ, sizeof(void*));
  arr->length = 0;
  arr->capacity = ARRAY_INIT_SZ;
}

int Array_find(Array* this, void* org,
                bool (*pred)(void* org,void* elem)) {
  for (int i = 0; i < this->length; i++) {
    void* elem;
    Array_get(this, i, &elem);
    if (pred(org, elem)) {
      return i;
    }
  }
  return -1;
}

void Object_freecb(void* elem) {
  Object* obj = (Object*) elem;
  obj->free(obj);
}

void Double_freecb(void* elem) {
  free(elem);
}
