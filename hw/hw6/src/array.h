#ifndef ARRAY_H
#define ARRAY_H

#include <stdbool.h>

#define ARRAY_INIT_SZ 8
struct Array {
  void** array;
  unsigned int length;
  unsigned int capacity;
} typedef Array;

void Array_init(Array*);
void Array_get(Array*, int, void**);
void Array_append(Array*, void*);
void Array_remove(Array*, int i, void (*free_cb)(void*));
void Array_clear(Array*, void (*free_cb)(void*));
int Array_find(Array*, void*, bool (*pred)(void* org,void* elem));

void Object_freecb(void*);
void Double_freecb(void*);

#endif /*ARRAY_H*/
