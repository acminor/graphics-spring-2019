#include <stdlib.h>
#include <stdio.h>
#include <glad/glad.h>
#include <cglm/cglm.h>
#include "planet_reader.h"

void pr_read_planet_data(char* data_path, Array** data) {
  printf("in planet reader\n");
  FILE* data_file = fopen(data_path, "r");
  GLchar* buff;
  mn_read_file(&buff, data_file);
  fclose(data_file);

  (*data) = malloc(sizeof(Array));
  Array_init((*data));

  volatile char c = buff[0];
  int i = 0;
  char temp[512];
  int j = 0;
  volatile PlanetData* planet;
  while (c != '\0') {
    switch (c) {
    case '#':
      while (c != '\n' && c != '\0') {
        i++;
        c = buff[i];
      }
      break;
    case '\n':
    case '\r':
      i++;
      break;
    default:
      planet = malloc(sizeof(PlanetData));
      for (j = i; buff[j] != ' '; j++) {}
      planet->name = malloc((j-i+1)*sizeof(char));
      memcpy(planet->name, &buff[i], (j-i));
      planet->name[j-i] = 0;

      i = j+1;
      i+=mn_po_read_num_to_buff(temp, &buff[i])+1;
      sscanf(temp, "%f", &planet->rmin);
      i+=mn_po_read_num_to_buff(temp, &buff[i])+1;
      sscanf(temp, "%f", &planet->rmax);
      i+=mn_po_read_num_to_buff(temp, &buff[i])+1;
      sscanf(temp, "%f", &planet->earth_radius);

      for (j = i; buff[j] != '\n'; j++) {}
      planet->texture = malloc((j-i+1)*sizeof(char));
      memcpy(planet->texture, &buff[i], (j-i));
      planet->texture[j-i] = 0;
      i = j;

      Array_append((*data), (void*) planet);
      break;
    }
    c = buff[i];
  }

  free(buff);
}
