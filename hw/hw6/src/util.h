#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>

void mn_read_file(GLchar** buff, FILE* file);
void gen_rnd_color(vec3 color);
int mn_po_read_num_to_buff(GLchar* buff, GLchar* str);

#endif /* UTIL_H */
