#ifndef BEIZER_SURFACE_H
#define BEIZER_SURFACE_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct BeizerSurface;
struct BeizerSurfaceArgs;

void init_BeizerSurface(struct Object*,
                 struct BeizerSurfaceArgs);
void new_BeizerSurface(struct Object**,
                struct BeizerSurfaceArgs);
void BeizerSurface_render(struct Object*, Camera*);
void BeizerSurface_free(struct Object*);
void BeizerSurface_update(Object*, SceneState*);

struct BeizerSurfaceArgs {
  ConfigurableShader* shader;
} typedef BeizerSurfaceArgs;

struct BeizerSurface {
  GLuint vao;
  GLuint vbo;
  bool wf;
  ConfigurableShader* shader;
  ConfigurableShader* point_shader;
  GLuint tex;
} typedef BeizerSurface;

#endif /* BEIZER_SURFACE_H */
