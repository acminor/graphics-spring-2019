#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "shader.h"
#include "config_shader.h"
#include "object.h"
#include "util.h"
#include "camera.h"
#include "beizer_surface.h"

#define WIDTH 1280
#define HEIGHT 800

void init_BeizerSurface(Object* container,
                           BeizerSurfaceArgs args) {
  BeizerSurface *self = malloc(sizeof(BeizerSurface));
  // CITE: sprite matrix creates a square
  //       plane and properly maps to texture
  //       data. This set of vertices and
  //       coordinates comes from the offline
  //       version of the learnopengl.com tutorial
  //       for rendering sprites. You can download the
  //       pdf on their site. It is chapter 47.
  GLfloat controlPoints[] = {
    /* pos */
    0.0, 20.0, 0.0,
    0.0, 10.0, 1.0,
    0.0, 0.0, 2.0,
    0.0, 0.0, 3.0,
    0.0, 0.0, 4.0,
    20.0, 0.0, 5.0,
    10.0, 0.0, 6.0,
    0.0, 0.0, 7.0,
    0.0, 0.0, 8.0,
    0.0, 0.0, 9.0,
    0.0, 0.0, 10.0,
    0.0, 0.0, 11.0,
    0.0, 0.0, 12.0,
    0.0, 0.0, 13.0,
    0.0, 0.0, 14.0,
    0.0, 0.0, 15.0,
    0.0, 0.0, 16.0,
    0.0, 0.0, 17.0,
    0.0, 0.0, 18.0,
    0.0, 0.0, 19.0,
    0.0, 0.0, 20.0,
    0.0, 0.0, 21.0,
    0.0, 0.0, 22.0,
    0.0, 0.0, 23.0,
    0.0, 0.0, 24.0,
  };

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(controlPoints),
               controlPoints, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE,
                        3*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  self->shader = args.shader;
  self->wf = false;

  {
    Shader* shader;
    new_Shader(&shader, "res/bz.vert.glsl", "res/bz.frag2.glsl");
    new_ConfigurableShader(&self->point_shader, shader);
  }

  int width, height;
  unsigned char* image =
    SOIL_load_image("res/background.png", &width, &height,
                    0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->tex);
  glBindTexture(GL_TEXTURE_2D, self->tex);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);

  IODesc obj_desc = {
    .free_addr=BeizerSurface_free,
    .render_addr=BeizerSurface_render,
    .update_addr=BeizerSurface_update
  };

  init_Object(container, (void*) self, obj_desc);
}

void new_BeizerSurface(Object** container,
                          BeizerSurfaceArgs args) {
  (*container) = malloc(sizeof(Object));
  init_BeizerSurface((*container), args);
}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE 0.1f
#define ROT_STEP_SIZE 10.0f
void BeizerSurface_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  BeizerSurface *self = (BeizerSurface*) container->object;

  UPDATE_CASE(is, key_1)
    self->wf = true;
  UPDATE_CASE(is, key_2)
    self->wf = false;
  UPDATE_CASE(is, key_7)
    self->shader->uInner0--;
  UPDATE_CASE(is, key_8)
    self->shader->uInner0++;
  UPDATE_CASE(is, key_9)
    self->shader->uInner1--;
  UPDATE_CASE(is, key_0)
    self->shader->uInner1++;
}

void BeizerSurface_render(Object* container, Camera* camera) {
  BeizerSurface *self = (BeizerSurface*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);

  if (self->wf) {
    self->shader->wireframe = true;
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
  } else {
    self->shader->wireframe = false;
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  }

  self->shader->texture = self->tex;
  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  glPatchParameteri(GL_PATCH_VERTICES, 5*5);
  glDrawArrays(GL_PATCHES, 0, 5*5);

  glm_mat4_copy(self->shader->model, self->point_shader->model);
  glm_mat4_copy(self->shader->projection, self->point_shader->projection);
  self->point_shader->use(self->point_shader);
  glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
  glDrawArrays(GL_POINTS, 0, 5*5);

  glBindVertexArray(0);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void BeizerSurface_free(Object* container) {
  BeizerSurface *self = (BeizerSurface*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
