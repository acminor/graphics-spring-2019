#ifndef CAMERA_H
#define CAMERA_H

/*
 * This is an almost direct translation of the TA's code from
 * here tutorial projects. It has been modified to work using
 * cglm and C. Furthermore, it has been simiplified and optimized
 * differently. We moved the semantics of updating the matrices.
 * We opted for update on change and not update on read.
 */

#include <cglm/cglm.h>
#include <GLFW/glfw3.h>
#include "input.h"

#define YAW -90.0f
#define PITCH 0.0f
#define SPEED 0.5f
#define SENSITIVITY 0.25f
#define ZOOM 45.0f
#define X 0
#define Y 1
#define Z 2
#define WIDTH_ 1280
#define HEIGHT_ 800

typedef struct SceneState SceneState;

/*
  Define camera in what seems like a logical way
  by combining view with camera. As every camera
  should have a projection. Self might not always
  make sense though.
 */
struct Camera {
  mat4 view;
  mat4 projection;
  vec3 position;
  vec3 front;
  vec3 up;
  vec3 right;
  vec3 world_up;
  float yaw;
  float pitch;
  float movement_speed;
  float mouse_sensitivity;
  float zoom;
} typedef Camera;

void Camera_update_vectors(Camera* self);
void Camera_update_projection(Camera* self);
void Camera_update(Camera* self, struct SceneState is);
void new_Camera(Camera** self);
void init_Camera(Camera* self);


#endif /* CAMERA_H */
