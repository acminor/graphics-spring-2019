#ifndef SCENE_H
#define SCENE_H

#include "object.h"
#include "input.h"
#include "camera.h"

struct SceneState {
  InputState input_state;
  float fps;
  // NOTE could put a current camera here
} typedef SceneState;

// NOTE for now ignored
struct SceneCameras {
  Camera cameras[128];
  int number_of_cameras;
} typedef _SceneCameras;

struct SceneObjects {
  Object objs[128];
  int number_of_objects;
} typedef _SceneObjects;

struct Scene {
  //_SceneCameras cameras; NOTE for now just one camera
  SceneState state;
  Camera camera;
  _SceneObjects objects;
  void (*render)(struct Scene*);
  void (*update)(struct Scene*, SceneState);
  void (*free)(struct Scene*);
} typedef Scene;

void init_Scene(Scene*);
void new_Scene(Scene**);
void Scene_update(Scene*, SceneState);
void Scene_render(Scene*);
void Scene_free(Scene*);

#endif /* SCENE_H */
