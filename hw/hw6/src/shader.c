#include <cglm/cglm.h>
#include "shader.h"

void new_Shader(Shader** self, const GLchar* vertexPath,
                const GLchar* fragmentPath) {
  (*self) = malloc(sizeof(Shader));
  init_Shader((*self), vertexPath, fragmentPath);
}

void init_Shader(Shader* self, const GLchar* vertexPath,
                 const GLchar* fragmentPath) {
  FILE *vShaderFile, *fShaderFile;
  GLchar *vShaderCode, *fShaderCode;
  GLuint vertex, fragment;
  GLint status;
  GLchar infoLog[512];

  // 1. Retrieve the vertex/fragment source code from filePath
  vShaderFile = fopen(vertexPath, "r");
  fShaderFile = fopen(fragmentPath, "r");
  status = (vShaderFile != 0) && (fShaderFile != 0);
	if (!status) {
    printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
    exit(-1);
  }
  mn_read_file(&vShaderCode, vShaderFile);
  mn_read_file(&fShaderCode, fShaderFile);
  // close file handlers
  fclose(vShaderFile);
  fclose(fShaderFile);
  // 2. Compile shaders
  // Vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, (const GLchar**)&vShaderCode, NULL);
  glCompileShader(vertex);
  // Print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
  }
  // Fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, (const GLchar**)&fShaderCode, NULL);
  glCompileShader(fragment);
  // Print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Shader Program
  self->program = glCreateProgram();
  glAttachShader(self->program, vertex);
  glAttachShader(self->program, fragment);
  glLinkProgram(self->program);
  // Print linking errors if any
  if ((glGetProgramiv(self->program, GL_LINK_STATUS, &status), !status)) {
    glGetProgramInfoLog(self->program, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Delete the shaders as they're linked into our program now and no longer necessary
  glDeleteShader(vertex);
  glDeleteShader(fragment);

  /* Attach functions to our shader object */
  self->use = shader_use;

  /* free temporary buffers and variables */
  free(vShaderCode);
  free(fShaderCode);
}

void init_TessShader(Shader* self,
                     const GLchar* vertexPath,
                     const GLchar* fragmentPath,
                     const GLchar* tcsPath,
                     const GLchar* tesPath) {
  FILE *vShaderFile, *fShaderFile, *tcsShaderFile, *tesShaderFile;
  GLchar *vShaderCode, *fShaderCode, *tcsShaderCode, *tesShaderCode;
  GLuint vertex, fragment, tcs, tes;
  GLint status;
  GLchar infoLog[512];

  // 1. Retrieve the vertex/fragment source code from filePath
  vShaderFile = fopen(vertexPath, "r");
  fShaderFile = fopen(fragmentPath, "r");
  tcsShaderFile = fopen(tcsPath, "r");
  tesShaderFile = fopen(tesPath, "r");
  status = (vShaderFile != 0) && (fShaderFile != 0)
    && (tcsShaderFile != 0) && (tesShaderFile != 0);
	if (!status) {
    printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
    exit(-1);
  }
  mn_read_file(&vShaderCode, vShaderFile);
  mn_read_file(&fShaderCode, fShaderFile);
  mn_read_file(&tcsShaderCode, tcsShaderFile);
  mn_read_file(&tesShaderCode, tesShaderFile);
  // close file handlers
  fclose(vShaderFile);
  fclose(fShaderFile);
  fclose(tcsShaderFile);
  fclose(tesShaderFile);
  // 2. Compile shaders
  // Vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, (const GLchar**)&vShaderCode, NULL);
  glCompileShader(vertex);
  // Print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
  }
  // Fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, (const GLchar**)&fShaderCode, NULL);
  glCompileShader(fragment);
  // Print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // tcs Shader
  tcs = glCreateShader(GL_TESS_CONTROL_SHADER);
  glShaderSource(tcs, 1, (const GLchar**)&tcsShaderCode, NULL);
  glCompileShader(tcs);
  // Print compile errors if any
  glGetShaderiv(tcs, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(tcs, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // tes Shader
  tes = glCreateShader(GL_TESS_EVALUATION_SHADER);
  glShaderSource(tes, 1, (const GLchar**)&tesShaderCode, NULL);
  glCompileShader(tes);
  // Print compile errors if any
  glGetShaderiv(tes, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(tes, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Shader Program
  self->program = glCreateProgram();
  glAttachShader(self->program, vertex);
  glAttachShader(self->program, tcs);
  glAttachShader(self->program, tes);
  glAttachShader(self->program, fragment);
  glLinkProgram(self->program);
  // Print linking errors if any
  if ((glGetProgramiv(self->program, GL_LINK_STATUS, &status), !status)) {
    glGetProgramInfoLog(self->program, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Delete the shaders as they're linked into our program now and no longer necessary
  glDeleteShader(vertex);
  glDeleteShader(fragment);
  glDeleteShader(tcs);
  glDeleteShader(tes);

  /* Attach functions to our shader object */
  self->use = shader_use;

  /* free temporary buffers and variables */
  free(vShaderCode);
  free(fShaderCode);
  free(tcsShaderCode);
  free(tesShaderCode);
}

void new_TessShader(Shader** self,
                     const GLchar* vertexPath,
                     const GLchar* fragmentPath,
                     const GLchar* tcsPath,
                     const GLchar* tesPath) {
  (*self) = malloc(sizeof(Shader));
  init_TessShader((*self), vertexPath, fragmentPath, tcsPath, tesPath);
}

void init_GeoShader(Shader* self,
                    const GLchar* vertexPath,
                    const GLchar* fragmentPath,
                    const GLchar* geometryPath) {
  FILE *vShaderFile, *fShaderFile, *gShaderFile;
  GLchar *vShaderCode, *fShaderCode, *gShaderCode;
  GLuint vertex, fragment, geometry;
  GLint status;
  GLchar infoLog[512];

  // 1. Retrieve the vertex/fragment source code from filePath
  vShaderFile = fopen(vertexPath, "r");
  fShaderFile = fopen(fragmentPath, "r");
  if (geometryPath != NULL)
    gShaderFile = fopen(geometryPath, "r");
  status = (vShaderFile != 0) && (fShaderFile != 0);
	if (!status) {
    printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
    exit(-1);
  }
  mn_read_file(&vShaderCode, vShaderFile);
  mn_read_file(&fShaderCode, fShaderFile);
  if (geometryPath != NULL)
    mn_read_file(&gShaderCode, gShaderFile);
  // close file handlers
  fclose(vShaderFile);
  fclose(fShaderFile);
  if (geometryPath != NULL)
    fclose(gShaderFile);
  // 2. Compile shaders
  // Vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, (const GLchar**)&vShaderCode, NULL);
  glCompileShader(vertex);
  // Print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
  }
  // Fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, (const GLchar**)&fShaderCode, NULL);
  glCompileShader(fragment);
  // Print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Geometry Shader
  if (geometryPath != NULL) {
    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, (const GLchar**)&gShaderCode, NULL);
    glCompileShader(geometry);
    // Print compile errors if any
    glGetShaderiv(geometry, GL_COMPILE_STATUS, &status);
    if (!status) {
      glGetShaderInfoLog(geometry, 512, NULL, infoLog);
      printf("ERROR::SHADER::GEOMETRY::COMPILATION_FAILED\n%s\n", infoLog);
      exit(-1);
    }
  }
  // Shader Program
  self->program = glCreateProgram();
  glAttachShader(self->program, vertex);
  glAttachShader(self->program, fragment);
  if (geometryPath != NULL)
    glAttachShader(self->program, geometry);
  glLinkProgram(self->program);
  // Print linking errors if any
  if ((glGetProgramiv(self->program, GL_LINK_STATUS, &status), !status)) {
    glGetProgramInfoLog(self->program, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Delete the shaders as they're linked into our program now and no longer necessary
  glDeleteShader(vertex);
  glDeleteShader(fragment);
  if (geometryPath != NULL)
    glDeleteShader(geometry);

  /* Attach functions to our shader object */
  self->use = shader_use;

  /* free temporary buffers and variables */
  free(vShaderCode);
  free(fShaderCode);
  if (geometryPath != NULL)
    free(gShaderCode);
}

void new_GeoShader(Shader** self,
                   const GLchar* vertexPath,
                   const GLchar* fragmentPath,
                   const GLchar* geometryPath) {
  (*self) = malloc(sizeof(Shader));
  init_GeoShader((*self), vertexPath, fragmentPath, geometryPath);
}

void shader_use(Shader* shader) {
  glUseProgram(shader->program);
}
