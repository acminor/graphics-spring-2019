#include <glad/glad.h>
#include <SOIL/SOIL.h>
#include "config_shader.h"

void init_ConfigurableShader(ConfigurableShader* self,
                             Shader* shader, char* texture_path) {
  self->shader = shader;
  self->use = ConfigurableShader_use;

  glm_mat4_identity(self->model);
  glm_mat4_identity(self->view);
  glm_mat4_identity(self->projection);

  int width, height;
  unsigned char* image =
    SOIL_load_image(texture_path, &width, &height, 0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->texture);
  glBindTexture(GL_TEXTURE_2D, self->texture);

  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);
}

void new_ConfigurableShader(ConfigurableShader** this,
                            Shader* shader, char* texture_path) {
  (*this) = malloc(sizeof(ConfigurableShader));
  init_ConfigurableShader((*this), shader, texture_path);
}

void ConfigurableShader_use(ConfigurableShader* self) {
  self->shader->use(self->shader);

  GLint modelLoc = glGetUniformLocation(self->shader->program,
                                        "model");
  GLint viewLoc = glGetUniformLocation(self->shader->program,
                                       "view");
  GLint projLoc = glGetUniformLocation(self->shader->program,
                                       "projection");
  GLint colorLoc = glGetUniformLocation(self->shader->program,
                                        "colorIn");
  glUniformMatrix4fv(modelLoc, 1, GL_FALSE, (GLfloat*) self->model);
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, (GLfloat*) self->view);
  glUniformMatrix4fv(projLoc, 1, GL_FALSE, (GLfloat*) self->projection);
  glUniform3fv(colorLoc, 1, (GLfloat*) self->color);

  glBindTexture(GL_TEXTURE_2D, self->texture);
}
