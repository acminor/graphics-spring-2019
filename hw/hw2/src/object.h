#ifndef OBJECT_H
#define OBJECT_H

#include <stdlib.h>
#include "camera.h"

struct SceneState;
struct Object {
  void* object;
  char* name;
  int type;
  void (*free)(struct Object*);
  void (*render)(struct Object*, Camera*);
  void (*update)(struct Object*, struct SceneState*);
} typedef Object;

enum {
      OBJ_NORMAL = 0,
      OBJ_POSITIONALABLE = 1,
      OBJ_ROTATABLE = 2
};

struct IODesc { // NOTE internal object descriptor
  int type;
  void (*free_addr)(struct Object*);
  void (*render_addr)(struct Object*, Camera*);
  void (*update_addr)(struct Object*, struct SceneState*);
  mat4 model;
} typedef IODesc;

void init_Object(Object* obj, void* io, IODesc io_desc);

#endif /* OBJECT_H */
