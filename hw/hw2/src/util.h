#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <glad/glad.h>

void mn_read_file(GLchar** buff, FILE* file);
void gen_rnd_color(vec3 color);

#define ARRAY_INIT_SZ 8
struct Array {
  void** array;
  unsigned int length;
  unsigned int capacity;
} typedef Array;

void Array_init(Array*);
void Array_get(Array*, int, void**);
void Array_append(Array*, void*);
void Array_remove(Array*, int i, void (*free_cb)(void*));
void Array_clear(Array*, void (*free_cb)(void*));

void Object_freecb(void*);

#endif /* UTIL_H */
