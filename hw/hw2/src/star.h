#ifndef STAR_H
#define STAR_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Star;
struct StarArgs;

void init_Star(struct Object*,
               struct StarArgs);
void new_Star(struct Object**,
              struct StarArgs);
void Star_recolor(struct Object*);
void Star_render(struct Object*, Camera*);
void Star_free(struct Object*);
void Star_update(Object*, SceneState*);
void Star_fermat_spiral(struct Star*, SceneState*);
void Star_arch_spiral(struct Star*, SceneState*);
void Star_log_spiral(struct Star*, SceneState*);

enum StarType {
    FERMAT_SPIRAL=1,
    LOGARITHMIC_SPIRAL=2,
    ARCHIMEDES_SPIRAL=3
};

struct StarArgs {
  ConfigurableShader* shader;
  int frame_lifetime;
  int star_type;
  bool fermat_direction;
} typedef StarArgs;

struct Star {
  GLuint vao;
  GLuint vbo;
  ConfigurableShader* shader;
  int star_type;
  int frame_lifetime;
  int start_frame;
  bool _fermat_direction;
  bool _has_started;
} typedef Star;

#endif /* STAR_H */
