#ifndef CONFIG_SHADER_H
#define CONFIG_SHADER_H

#include <cglm/cglm.h>
#include <stdbool.h>

#include "shader.h"

struct ConfigurableShader {
  Shader* shader;
  mat4 model;
  mat4 view;
  mat4 projection;
  float color[3];
  GLuint texture;
  void (*use)(struct ConfigurableShader*);
} typedef ConfigurableShader;

void ConfigurableShader_use(ConfigurableShader*);
void init_ConfigurableShader(ConfigurableShader*, Shader*, char* texture_path);
void new_ConfigurableShader(ConfigurableShader**, Shader*, char* texture_path);

#endif /* CONFIG_SHADER_H */
