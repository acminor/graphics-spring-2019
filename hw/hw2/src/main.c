#include <stdio.h>

// GLAD
#include <glad/glad.h>

// GLFW
#include <GLFW/glfw3.h>

// GLM Mathematics
#include <cglm/cglm.h>

#include <time.h>
#include <stdlib.h>

// Other includes
#include "scene.h"
#include "shader.h"
#include "config_shader.h"
#include "object.h"
#include "random_face_object.h"
#include "input.h"

// Function prototypes
void key_callback(GLFWwindow* window, int key,
                  int scancode, int action, int mode);

// Window dimensions
const GLuint WIDTH = 1280, HEIGHT = 800;

// NOTE: Empty define to make searching for globals easier
#define GLOBAL

//GLOBAL InputState globalInputState;
GLOBAL InputState globalInputState;

// The MAIN function, from here we start
// the application and run the game loop
int main()
{
  /* NOTE: only place in the program
     where the generator should be
     initialized */
  srand(time(NULL));
  init_InputState(&globalInputState);

	// Init GLFW
	glfwInit();
	// Set all the required options for GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

	// Create a GLFWwindow object that we can use for GLFW's functions
	GLFWwindow* window = glfwCreateWindow(WIDTH, HEIGHT,
                                        "LearnOpenGL", NULL, NULL);
	glfwMakeContextCurrent(window);

	// Set the required callback functions
	glfwSetKeyCallback(window, key_callback);

	// Initialize GLAD to setup the OpenGL Function pointers
	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		printf("Failed to initialize glad\n");
		return -1;
	};

	// Define the viewport dimensions
	glViewport(0, 0, WIDTH, HEIGHT);

	// Setup OpenGL options
	glEnable(GL_DEPTH_TEST);
  glPointSize(2);
  glLineWidth(2);

  Scene our_scene;
  unsigned int current_frame;
  init_Scene(&our_scene);

	// Game loop
	while (!glfwWindowShouldClose(window))
	{
		// Check if any events have been activiated (key pressed, mouse moved etc.) and call corresponding response functions
		glfwPollEvents();

		// Render
		// Clear the colorbuffer
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    SceneState state;
    state.input_state = globalInputState;
    state.current_frame = current_frame;
    //our_obj.update(&our_obj, &state);
    //our_obj.render(&our_obj, &temp);
    our_scene.update(&our_scene, state);
    our_scene.render(&our_scene);
    current_frame++;

		// Swap the screen buffers
		glfwSwapBuffers(window);
	}

	// Terminate GLFW, clearing any resources allocated by GLFW.
  //our_obj.free(&our_obj);
	glfwTerminate();
	return 0;
}

void key_callback(GLFWwindow* window, int key,
                  int scancode, int action, int mode){
  RandomFaceObject our_obj;
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);

  switch(key) {
  case GLFW_KEY_1:
    globalInputState.key_1 = action;
    break;
  case GLFW_KEY_2:
    globalInputState.key_2 = action;
    break;
  case GLFW_KEY_3:
    globalInputState.key_3 = action;
    break;
  case GLFW_KEY_4:
    globalInputState.key_4 = action;
    break;
  case GLFW_KEY_0:
    globalInputState.key_0 = action;
    break;
  case GLFW_KEY_W: /* forward (away) */
    globalInputState.key_w = action;
    break;
  case GLFW_KEY_S: /* backwards (towards) */
    globalInputState.key_s = action;
    break;
  case GLFW_KEY_A: /* left */
    globalInputState.key_a = action;
    break;
  case GLFW_KEY_D: /* right */
    globalInputState.key_d = action;
    break;
  case GLFW_KEY_E: /* down */
    globalInputState.key_e = action;
    break;
  case GLFW_KEY_Q: /* up */
    globalInputState.key_q = action;
    break;
  case GLFW_KEY_U: /* rotate x ccw */
    globalInputState.key_u = action;
    break;
  case GLFW_KEY_I: /* rotate x cw */
    globalInputState.key_i = action;
    break;
  case GLFW_KEY_J: /* rotate y ccw */
    globalInputState.key_j = action;
    break;
  case GLFW_KEY_K: /* rotate y cw */
    globalInputState.key_k = action;
    break;
  case GLFW_KEY_O: /* rotate z ccw */
    globalInputState.key_o = action;
    break;
  case GLFW_KEY_P: /* rotate z cw */
    globalInputState.key_p = action;
    break;
  }
}
