#ifndef SCENE_H
#define SCENE_H

#include "object.h"
#include "camera.h"
#include "input.h"
#include "util.h"

struct SceneState {
  InputState input_state;
  float fps;
  unsigned int current_frame;
  // NOTE could put a current camera here
} typedef SceneState;

// NOTE for now ignored
struct SceneCameras {
  Camera cameras[128];
  int number_of_cameras;
} typedef _SceneCameras;

#define NUM_OBJS 128
struct Scene {
  //_SceneCameras cameras; NOTE for now just one camera
  SceneState state;
  Camera camera;
  Array objects;
  bool _current_fermat_direction;
  int spiral_type;
  void (*render)(struct Scene*);
  void (*update)(struct Scene*, SceneState);
  void (*free)(struct Scene*);
} typedef Scene;

void init_Scene(Scene*);
void new_Scene(Scene**);
void Scene_update(Scene*, SceneState);
void Scene_render(Scene*);
void Scene_free(Scene*);
void Scene__delete_expired_particles(Scene* self);

#endif /* SCENE_H */
