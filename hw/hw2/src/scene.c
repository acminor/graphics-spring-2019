#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "scene.h"
#include "input.h"
#include "object.h"
#include "star.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

void gen_star(Scene* self) {
  // FIXME should use a global shader per shader system for performance
  {
    Shader* our_shader;
    new_Shader(&our_shader, "res/sprite.vert.glsl", "res/sprite.frag.glsl");
    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader, "res/Star.png");

    StarArgs args = {
      .frame_lifetime=600,
      .shader=our_conf_shader,
      .star_type=self->spiral_type,
      .fermat_direction=!self->_current_fermat_direction
    };

    Object* star;
    new_Star(&star, args);
    Array_append(&self->objects, (void*) star);
    self->_current_fermat_direction = !self->_current_fermat_direction;
  }
}

void Scene__delete_expired_particles(Scene* self) {
    for (int i = 0; i < self->objects.length; i++) {
        Object *obj;
        Array_get(&self->objects, i, (void**) &obj);
        if (((Star*) obj->object)->frame_lifetime == 0) {
            Array_remove(&self->objects, i, Object_freecb);
        }
    }
}

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  self->spiral_type = ARCHIMEDES_SPIRAL;
  init_InputState(&(self->state.input_state));
  Array_init(&self->objects);
  self->_current_fermat_direction = false;

  glm_mat4_identity(self->camera.view);
  glm_mat4_identity(self->camera.projection);
  glm_translate(self->camera.view, (vec3){0.0f,0.0f,-40.0f});
  glm_perspective(glm_rad(45.0f), (GLfloat)WIDTH/(GLfloat)HEIGHT,
                  0.1f, 100.0f, self->camera.projection);
  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Scene_update(Scene* self, SceneState state) {
  InputState is = state.input_state;
  UPDATE_CASE(is,key_1)
    {
      Array_clear(&self->objects, Object_freecb);
      self->spiral_type = ARCHIMEDES_SPIRAL;
    }
  UPDATE_CASE(is,key_2)
    {
      Array_clear(&self->objects, Object_freecb);
      self->spiral_type = FERMAT_SPIRAL;
    }
  UPDATE_CASE(is,key_3)
    {
      Array_clear(&self->objects, Object_freecb);
      self->spiral_type = LOGARITHMIC_SPIRAL;
    }

  self->state = state;
  if (state.current_frame % 30 == 0) {
    gen_star(self);
  }

  Scene__delete_expired_particles(self);

  int num_objs = self->objects.length;
  for (int i = 0; i < num_objs; i++) {
    Object *obj;
    Array_get(&self->objects, i, ((void**) &obj));
    obj->update(obj, &self->state);
  }
}

void Scene_render(Scene* self) {
  int num_objs = self->objects.length;
  for (int i = 0; i < num_objs; i++) {
    Object *obj;
    Array_get(&self->objects, i, ((void**) &obj));
    obj->render(obj, &self->camera);
  }
}
