#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdio.h>
#include <stdlib.h>
#include "util.h"
#include "object.h"

void mn_read_file(GLchar** buff, FILE* file) {
  struct _resize_buff {
    struct _resize_buff* next;
    GLchar buff[128];
  } typedef _resize_buff;
  GLchar cc; /* current character */
  volatile GLuint count, i;
  _resize_buff *temp, *tail, *head;

  head = malloc(sizeof(_resize_buff));
  head->next = NULL;
  tail = head;

  for (count = 0; cc != EOF; count++) {
    if (((count % 128) == 0) && (count != 0)) {
      temp = malloc(sizeof(_resize_buff));
      temp->next = NULL;

      tail->next = temp;
      tail = tail->next;
    }

    tail->buff[count%128] = cc = fgetc(file);
  }

  /* I believe -1 is right if I remember finalization of loop properly */
  /* might crash if file is a certain size due to buff exhaustion FIXME */
  tail->buff[(count%128)-1] = '\0';

  (*buff) = malloc(sizeof(GLchar)*count);
  for (i = 0; i < count; i++) {
    if (((i % 128) == 0) && (i != 0)) {
      temp = head;
      head = head->next;
      free(temp); /* clean up incrementally */
    }
    (*buff)[i] = head->buff[i%128];
  }

  free(head);
}

/*
 * Internal helper function to help implement
 * the HSL to RGB conversion discussed here
 * https://en.wikipedia.org/wiki/HSL_and_HSV
 */
float mn_int_gnc_chroma_convert_fn(int n, float hue, float sat,
                                   float lightness) {
  float k = fmod(n + (hue/30.0f), 12.0f);
  float a = sat*((lightness < 1.0f-lightness)
                 ? lightness : 1.0f-lightness);

  float min_val = k-3.0f;
  min_val = (9.0f-k < min_val) ? 9.0f-k : min_val;
  min_val = (1.0f < min_val) ? 1.0f : min_val;

  float max_val = (min_val > -1.0f) ? min_val : -1.0f;
  return lightness - a*max_val;
}

/*
 * Implements the HSL to RGB conversion discussed here
 * https://en.wikipedia.org/wiki/HSL_and_HSV
 *
 * HSL was chosen as it more naturally generates random colors
 * without causing everything to have a high lightness value.
 */
void gen_rnd_color(vec3 color) {
  float hue = (rand() % 36000)/100.0f;
  float sat = 1.0f;
  float lightness = .5f;
  color[0] = mn_int_gnc_chroma_convert_fn(0, hue, sat, lightness);
  color[1] = mn_int_gnc_chroma_convert_fn(8, hue, sat, lightness);
  color[2] = mn_int_gnc_chroma_convert_fn(4, hue, sat, lightness);
}

void Array_get(Array* arr, int i, void** elem) {
  if (i >= arr->length) {
    (*elem) = NULL;
  } else {
    (*elem) = arr->array[i];
  }
}

void Array_append(Array* arr, void* elem) {
  if (arr->length == arr->capacity) {
    arr->capacity *= 2;
    arr->array = realloc(arr->array, arr->capacity*sizeof(void*));
  }

  arr->array[arr->length] = elem;
  arr->length++;
}

void Array_remove(Array* arr, int i, void (*free_cb)(void*)) {
  if (i >= arr->length) {
    return;
  }

  free_cb(arr->array[i]);
  for (int j = i; j < arr->length-1; j++) {
    arr->array[j] = arr->array[j+1];
  }
  arr->length--;
}

void Array_clear(Array* arr, void (*free_cb)(void*)) {
  for (int i = 0; i < arr->length; i++) {
    free_cb(arr->array[i]);
  }

  arr->array = realloc(arr->array, ARRAY_INIT_SZ*sizeof(void*));
  arr->length = 0;
  arr->capacity = ARRAY_INIT_SZ;
}

void Array_init(Array* arr) {
  arr->array = calloc(ARRAY_INIT_SZ, sizeof(void*));
  arr->length = 0;
  arr->capacity = ARRAY_INIT_SZ;
}

void Object_freecb(void* elem) {
  Object* obj = (Object*) elem;
  obj->free(obj);
}
