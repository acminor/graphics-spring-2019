#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <time.h>

#include "math.h"
#include "config_shader.h"
#include "star.h"
#include "object.h"
#include "object_reader.h"
#include "util.h"
#include "camera.h"

#define WIDTH 1280
#define HEIGHT 800

void init_Star(Object* container,
               StarArgs args) {
  Star *self = malloc(sizeof(Star));

  // CITE: sprite matrix creates a square
  //       plane and properly maps to texture
  //       data. This set of vertices and
  //       coordinates comes from the offline
  //       version of the learnopengl.com tutorial
  //       for rendering sprites. You can download the
  //       pdf on their site. It is chapter 47.
  GLfloat sprite[] = {
   /*pos        tex*/
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f,
     0.0f, 0.0f, 0.0f, 0.0f,
     0.0f, 1.0f, 0.0f, 1.0f,
     1.0f, 1.0f, 1.0f, 1.0f,
     1.0f, 0.0f, 1.0f, 0.0f
  };

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(sprite),
               sprite, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE,
                        4*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);
  glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE,
                        4*sizeof(GLfloat), (GLvoid*)(2*sizeof(GLfloat)));
  glEnableVertexAttribArray(1);

  glBindVertexArray(0); /* unbind vao */

  float color[3];
  gen_rnd_color(color);

  self->shader = args.shader;
  self->shader->color[0] = color[0];
  self->shader->color[1] = color[1];
  self->shader->color[2] = color[2];
  self->star_type = args.star_type;
  self->frame_lifetime = args.frame_lifetime;
  self->_has_started = false;
  self->start_frame = 0;
  self->_fermat_direction = args.fermat_direction;

  IODesc obj_desc = {
    .free_addr=Star_free,
    .render_addr=Star_render,
    .update_addr=Star_update
  };

  init_Object(container, (void*) self, obj_desc);
}

void new_Star(Object** container,
              StarArgs args) {
  (*container) = malloc(sizeof(Object));
  init_Star((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {\
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Star_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  Star *our_obj = (Star*) container->object;

  if (!our_obj->_has_started) {
    our_obj->start_frame = state->current_frame;
    our_obj->_has_started = true;
  }

  switch(our_obj->star_type) {
  case FERMAT_SPIRAL:
    Star_fermat_spiral(our_obj, state);
    break;
  case LOGARITHMIC_SPIRAL:
    Star_log_spiral(our_obj, state);
    break;
  case ARCHIMEDES_SPIRAL:
    Star_arch_spiral(our_obj, state);
    break;
  }
  our_obj->frame_lifetime--;
}

void Star_render(Object* container, Camera* camera) {
  Star *self = (Star*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);
  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  glDrawArrays(GL_TRIANGLES, 0, 4*6);
  glBindVertexArray(0);
}

void Star_free(Object* container) {
  Star *self = (Star*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}

#define A_COEFF 1.0f
#define B_COEFF 1.0f

void Star_fermat_spiral(Star* our_obj, SceneState* state) {
  // FIXME need a better time factor here
  float theta = 0.038 * (state->current_frame - our_obj->start_frame);
  float x = A_COEFF*sqrt(theta)*cos(theta);
  float y = A_COEFF*sqrt(theta)*sin(theta);

  if (our_obj->_fermat_direction) {
    x = -x;
    y = -y;
  }

  mat4 temp;
  glm_mat4_identity(temp);
  glm_translate_to(temp,
                   (vec3){x, y, 0.0f},
                   our_obj->shader->model);
}

void Star_log_spiral(Star* our_obj, SceneState* state) {
  float theta = 0.02f * (state->current_frame - our_obj->start_frame);
  float x = A_COEFF*exp(0.2f*B_COEFF*theta)*cos(theta);
  float y = A_COEFF*exp(0.2f*B_COEFF*theta)*sin(theta);

  mat4 temp;
  glm_mat4_identity(temp);
  glm_translate_to(temp,
                   (vec3){x, y, 0.0f},
                   our_obj->shader->model);
}

void Star_arch_spiral(Star* our_obj, SceneState* state) {
  float theta = 0.02f * (state->current_frame - our_obj->start_frame);
  float x = (A_COEFF + B_COEFF*theta)*cos(theta);
  float y = (A_COEFF + B_COEFF*theta)*sin(theta);

  mat4 temp;
  glm_mat4_identity(temp);
  glm_translate_to(temp,
                   (vec3){x, y, 0.0f},
                   our_obj->shader->model);
}
