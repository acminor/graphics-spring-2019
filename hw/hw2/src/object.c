#include "object.h"

void init_Object(Object* obj, void* io, IODesc io_desc) {
  obj->object = io;
  obj->free   = io_desc.free_addr;
  obj->render = io_desc.render_addr;
  obj->update = io_desc.update_addr;
  obj->name = NULL;
}

void new_Object(Object** obj, void* io, IODesc io_desc) {
  (*obj) = malloc(sizeof(Object));
  init_Object((*obj), io, io_desc);
}
