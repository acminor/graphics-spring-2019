#version 330 core
layout (location = 0) in vec2 position;
layout (location = 1) in vec2 inTexCoord;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform vec3 colorIn;

out vec2 TexCoord;
out vec3 ourColor;

void main()
{
  gl_Position = projection * view * model * vec4(position, 0.0f, 1.0f);
  ourColor = colorIn;
  TexCoord = inTexCoord;
}
