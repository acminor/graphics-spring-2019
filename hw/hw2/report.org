#+TITLE: Homework 2 Build Document and Report
#+AUTHOR: Austin Minor (2018280284)

* Purpose
This document serves as a brief description of the process and layout
of the code along with how to build and run the code.
My incremental approach in Homework 1 has seemed to allow good reuse
in Homework2. I hope some of this may be used in latter projects and
homework.  

* Building the Code
As in Homework 1, this code is built using Autotools. This was chosen as
it provides a good platform for cross-platform building and reproduceable
builds. Their may be issues on using a non-Linus/Unix platform. This can
be mitigated by using a POSIX like environment such as CYGWIN.

Assuming your build environment is correct. You may need to install some
other packages for the program to compile. When these should be installed
or cannot be found, the Autotools configure scripts will alert you to this.

Assuming everything is properly installed and setup, then...
1) In a terminal in the root directory run "./configure"
2) If successful, run "make"

After the program is compiled call if from the root directory like
so "./src/program". The program must be ran from the root directory
as it will need to look for resources (textures, etc.)
in the local file structure.

NOTE: This code has only been tested to compile and run on Fedora Linux
      64-bit.

* Code History and Layout
This code continues in history from the Homework 1 code. In this code,
we turn our scene into a emitter of particles. We acknowledge a more
logical way would be to create an emitter object. However, we leave this
as a potential further refactoring of the code.

With this, most of our code stays the same. We added an Array data type
which will be useful for further homework and projects. We also created
a Star type which encompasses the sprite mapping tutorial in the
"learnopengl.com" offline PDF book. This was in chapter 47 of that book.
We mainly used their pre-calculated sprite VBO array for easy in applying
a texture to a plane. This is cited in the source code.

We further made some extensions and changes to the ConfigurableShader to
allow it to handle textures. This does break the RandomFaceObject; however,
in the future this could be refactored to not be an issue.

We did implement some further operations to make sure we are not leaking
memory with our Array implementation. This was not tested. We also still
do not yet free memory on exit of a scene. This is because memory is
automatically freed on exiting a program. For now, we do not load other
scenes. Thus, this memory can be freed on exit.

As in the last homework, the "src" folder is for source files, "res" folder
for resources such as textures or shaders, and "libs" folder is for some
external library dependencies.

NOTE: The work in Homework 1 was successful in creating an easier to extend
      base from which to do Homework 2.

* Program Mappings

The mappings for this program are simple and summarized below.

|-----+--------------------|
| Key | Action             |
|-----+--------------------|
|   1 | Archimedes Spiral  |
|   2 | Fermat Spiral      |
|   3 | Logarithmic Spiral |
|-----+--------------------|
