#ifndef CAMERA_H
#define CAMERA_H

#include <cglm/cglm.h>

/*
  Define camera in what seems like a logical way
  by combining view with camera. As every camera
  should have a projection. This might not always
  make sense though.
 */
struct Camera {
  mat4 view;
  mat4 projection;
} typedef Camera;

#endif /* CAMERA_H */
