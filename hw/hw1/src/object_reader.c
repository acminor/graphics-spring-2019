#include <cglm/cglm.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "object_reader.h"
#include "config_shader.h"
#include "util.h"

int mn_po_read_num_to_buff(GLchar* buff, GLchar* str) {
  char c = str[0];
  int i = 0;
  while(c != '\0' && c != '\n' && c != ' ' && c != '\r') {
    switch (c) { /* NOTE: does not support '+' prefix */
    case '0': case '1': case '2':
    case '3': case '4': case '5':
    case '6': case '7': case '8':
    case '9': case '-': case '.':
    case 'e':
      buff[i] = c;
      i++;
      c = str[i];
      break;
    default:
      printf("PARSING ERROR MN_PO_READ_NUM_TO_BUFF\n");
      buff[i+1] = '\0';
      printf("%s\n", buff);
      printf("%c\n", c);
      return -1; /* parsing error */
    }
  }

  buff[i] = '\0';
  return i;
}

void parse_obj(GLchar* obj_file_path, Mesh* out) {
  if (out == NULL) {
    printf("FAILURE: PASSED NULL POINTER TO PARSE_OBJ");
    exit(1);
  }

  /* NOTE: probably should make client init mesh
     but we will do it here for now */
  out->free = Mesh_free;
  out->number_of_faces = 0;

  struct {
    float** array;
    int len;
    int cap;
  } vtc;
  vtc.len = 0;
  vtc.cap = 100;
  vtc.array = malloc(vtc.cap*sizeof(float*));

  out->head = malloc(sizeof(Face));
  Face *current, *previous;
  current = previous = out->head;

  GLchar* sobj = NULL;
  FILE* obj_file = fopen(obj_file_path, "r");
  mn_read_file(&sobj, obj_file);
  fclose(obj_file);

  char c = sobj[0];
  int i = 0;
  while (c != '\0') {
    switch (c) {
      GLfloat x, y, z;
      GLchar buff[128];
      GLuint p1, p2, p3;
    case '#':
      while (c != '\n' && c != '\0') {
                                      i++;
                                      c = sobj[i];
      }
      /*if (c == '\n') {
                      i++;
                      c = sobj[i];
                      }*/
      break;
    case 'v':
      i+=2; /* NOTE: assumes only one space */
      /* NOTE: does no error checking on object file */
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%f", &x);
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%f", &y);
      i+=mn_po_read_num_to_buff(buff, sobj+i);
      sscanf(buff, "%f", &z);

      printf("Reading vertex: %f %f %f\n", x, y, z);

      vtc.array[vtc.len] = malloc(3*sizeof(float));
      vtc.array[vtc.len][0] = x;
      vtc.array[vtc.len][1] = y;
      vtc.array[vtc.len][2] = z;

      vtc.len++;
      if (vtc.len == vtc.cap) {
        vtc.cap += 100;
        vtc.array = realloc(vtc.array, vtc.cap*sizeof(vec3));
      }
      break;
    case 'f':
      i+=2; /* NOTE: assumes only one space */
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%i", &p1);
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%i", &p2);
      i+=mn_po_read_num_to_buff(buff, sobj+i);
      sscanf(buff, "%i", &p3);
      printf("Reading face: %d %d %d\n", p1, p2, p3);

      /* NOTE: obj vertices are indexed from 1 */
      current->face[0][0] = vtc.array[p1-1][0];
      current->face[0][1] = vtc.array[p1-1][1];
      current->face[0][2] = vtc.array[p1-1][2];
      current->face[1][0] = vtc.array[p2-1][0];
      current->face[1][1] = vtc.array[p2-1][1];
      current->face[1][2] = vtc.array[p2-1][2];
      current->face[2][0] = vtc.array[p3-1][0];
      current->face[2][1] = vtc.array[p3-1][1];
      current->face[2][2] = vtc.array[p3-1][2];
      out->number_of_faces++;

      current->next = malloc(sizeof(Face));

      previous = current;
      current = current->next;
      break;
    default:
      i++;
      break;
    }

    /* Get the current character */
    c = sobj[i];
  }

  /* NOTE: we always allocate for the next read
     this leaves an extra node allocated on finish
     get rid of this node */
  previous->next = NULL;
  free(current);
}

void Mesh_free(Mesh* mesh) {
  Face* current = mesh->head;
  Face* next = NULL;
  while (current != NULL) {
    next = current->next;
    free(current);
    current = next;
  }
}
