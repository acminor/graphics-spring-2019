#ifndef SHADER_H
#define SHADER_H

#include <stdio.h>
#include <stdlib.h>

#include <glad/glad.h>

void _read_file(GLchar** buff, FILE* file) {
  struct _resize_buff {
    struct _resize_buff* next;
    GLchar buff[128];
  } typedef _resize_buff;
  GLchar cc; /* current character */
  GLuint count, i;
  _resize_buff* temp, tail, head;

  for (count = 0; cc != EOF; count++) {
    if (((count % 128) == 0) && (count != 0)) {
      temp = malloc(sizeof(_resize_buff));
      temp->next = NULL;

      tail->next = temp;
      tail = tail->next;
    }

    tail->buff[count%128] = cc = fget(c);
  }

  /* I believe -1 is right if I remember finalization of loop properly */
  tail->buff[(count%128)-1] = '\0';

  (*buff) = malloc(sizeof(GLchar)*count);
  for (i = 0; i < count; i++) {
    if (((i % 128) == 0) && (i != 0)) {
      temp = head;
      head = head->next;
      free(temp); /* clean up incrementally */
    }

    (*buff)[i] = head->buff[i%128];
  }

  free(head);
}

struct Shader {
  GLuint program;
  void (*use)(Shader*);
} typedef Shader;

void new_Shader(Shader** this, const GLchar* vertexPath, const GLchar* fragmentPath) {
  (*this) = malloc(sizeof(Shader));
  init_Shader((*this), vertexPath, fragmentPath);
}

void init_Shader(Shader* this, const GLchar* vertexPath, const GLchar* fragmentPath) {
  const FILE* vShaderFile, fShaderFile;
  const GLchar* vShaderCode, fShaderCode;
  GLuint vertex, fragment;
  const GLint status;
  GLchar infoLog[512];

  // 1. Retrieve the vertex/fragment source code from filePath
  vShaderFile = fopen(vertexPath, "r");
  fShaderFile = fopen(fragmentPath, "r");
  status = (vShaderFile != 0) && (fShaderFile != 0);
  _read_file(&vShaderCode, vShaderFile);
  _read_file(&fShaderCoder, fShaderFile);
  // close file handlers
  fclose(vShaderFile);
  fclose(fShaderFile);
	if (!status) {
    printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
  }
  // 2. Compile shaders
  // Vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, &vShaderCode, NULL);
  glCompileShader(vertex);
  // Print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
  }
  // Fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, &fShaderCode, NULL);
  glCompileShader(fragment);
  // Print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
	}
  // Shader Program
  this->Program = glCreateProgram();
  glAttachShader(this->Program, vertex);
  glAttachShader(this->Program, fragment);
  glLinkProgram(this->Program);
  // Print linking errors if any
  if ((glGetProgramiv(this->Program, GL_LINK_STATUS, &success), !success)) {
    glGetProgramInfoLog(this->Program, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
	}
  // Delete the shaders as they're linked into our program now and no longer necessary
  glDeleteShader(vertex);
  glDeleteShader(fragment);

  /* Attach functions to our shader object */
  this->use = shader_use;

  /* free temporary buffers and variables */
  // TODO
  free(vShaderCode);
  free(fShaderCode);
}

void shader_use(Shader* shader) {
  glUseProgram(shader->program);
}

#endif /* SHADER_H */
