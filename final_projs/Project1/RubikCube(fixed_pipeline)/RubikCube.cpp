#include "RubikCube.h"
#include <iomanip>
#include <gl\glut.h>

double rotateSpeed = 1.0f;

//record which face of each sub-cube should face inside
bool hiddenFace[3][3][3][6] = {
	{{{1, 0, 0, 1, 0, 1}, {1, 0, 0, 0, 0, 1}, {1, 1, 0, 0, 0, 1}}, 
	{{1, 0, 0, 1, 0, 0}, {1, 0, 0, 0, 0, 0}, {1, 1, 0, 0, 0, 0}}, 
	{{1, 0, 0, 1, 1, 0}, {1, 0, 0, 0, 1, 0}, {1, 1, 0, 0, 1, 0}}}, 

	{{{0, 0, 0, 1, 0, 1}, {0, 0, 0, 0, 0, 1}, {0, 1, 0, 0, 0, 1}}, 
	{{0, 0, 0, 1, 0, 0}, {0, 0, 0, 0, 0, 0}, {0, 1, 0, 0, 0, 0}}, 
	{{0, 0, 0, 1, 1, 0}, {0, 0, 0, 0, 1, 0}, {0, 1, 0, 0, 1, 0}}}, 

	{{{0, 0, 1, 1, 0, 1}, {0, 0, 1, 0, 0, 1}, {0, 1, 1, 0, 0, 1}}, 
	{{0, 0, 1, 1, 0, 0}, {0, 0, 1, 0, 0, 0}, {0, 1, 1, 0, 0, 0}}, 
	{{0, 0, 1, 1, 1, 0}, {0, 0, 1, 0, 1, 0}, {0, 1, 1, 0, 1, 0}}}
};

/************************************************************************/
/* Constructor of RubikCube class
 Read this function carefully to understand the structure of the Rubik's cube*/
/************************************************************************/

RubikCube::RubikCube()
{
	memset(trackBall.MVmatrix, 0, 16*sizeof(double));
	trackBall.MVmatrix[0] = 1;
	trackBall.MVmatrix[5] = 1;
	trackBall.MVmatrix[10] = 1;
	trackBall.MVmatrix[15] = 1;
	
	int colors[6][3] = {{255, 0, 0},
	{255, 255, 0}, 
	{0, 255, 0}, 
	{0, 0, 255}, 
	{255, 122, 0}, 
	{255, 255, 255}};

	int faceSeq = 0;
	for (int i = 0;i < 3;++i)
	{
		for (int j = 0;j < 3;++j)
		{
			for (int k = 0;k < 3;++k)
			{
				magicCube[i][j][k].pos = Vector3D(cubeSize/2+i*cubeSize, 
					cubeSize/2+j*cubeSize, cubeSize/2+k*cubeSize);
				memset(magicCube[i][j][k].matrix, 0, 16*sizeof(double));
				magicCube[i][j][k].matrix[0] = 1;
				magicCube[i][j][k].matrix[5] = 1;
				magicCube[i][j][k].matrix[10] = 1;
				magicCube[i][j][k].matrix[15] = 1;

				for (int l = 0;l < 6;++l)
				{
					magicCube[i][j][k].faceColor[l].x = colors[l][0];
					magicCube[i][j][k].faceColor[l].y = colors[l][1];
					magicCube[i][j][k].faceColor[l].z = colors[l][2];
				}
			}
		}
	}

	//Construct layers rotate around x-axis
	for (int i = 0;i < 3;++i)
	{
		layers[0][i].cubes[0].x = i;layers[0][i].cubes[0].y = 0;layers[0][i].cubes[0].z = 0;
		layers[0][i].cubes[1].x = i;layers[0][i].cubes[1].y = 1;layers[0][i].cubes[1].z = 0;
		layers[0][i].cubes[2].x = i;layers[0][i].cubes[2].y = 2;layers[0][i].cubes[2].z = 0;
		layers[0][i].cubes[3].x = i;layers[0][i].cubes[3].y = 2;layers[0][i].cubes[3].z = 1;
		layers[0][i].cubes[4].x = i;layers[0][i].cubes[4].y = 2;layers[0][i].cubes[4].z = 2;
		layers[0][i].cubes[5].x = i;layers[0][i].cubes[5].y = 1;layers[0][i].cubes[5].z = 2;
		layers[0][i].cubes[6].x = i;layers[0][i].cubes[6].y = 0;layers[0][i].cubes[6].z = 2;
		layers[0][i].cubes[7].x = i;layers[0][i].cubes[7].y = 0;layers[0][i].cubes[7].z = 1;
		layers[0][i].cubes[8].x = i;layers[0][i].cubes[8].y = 1;layers[0][i].cubes[8].z = 1;
		layers[0][i].axis = Vector3D(1.0, 0.0, 0.0);
	}
	//Construct layers rotate around y-axis
	for (int i = 0;i < 3;++i)
	{
		layers[1][i].cubes[0].x = 0;layers[1][i].cubes[0].y = i;layers[1][i].cubes[0].z = 0;
		layers[1][i].cubes[1].x = 0;layers[1][i].cubes[1].y = i;layers[1][i].cubes[1].z = 1;
		layers[1][i].cubes[2].x = 0;layers[1][i].cubes[2].y = i;layers[1][i].cubes[2].z = 2;
		layers[1][i].cubes[3].x = 1;layers[1][i].cubes[3].y = i;layers[1][i].cubes[3].z = 2;
		layers[1][i].cubes[4].x = 2;layers[1][i].cubes[4].y = i;layers[1][i].cubes[4].z = 2;
		layers[1][i].cubes[5].x = 2;layers[1][i].cubes[5].y = i;layers[1][i].cubes[5].z = 1;
		layers[1][i].cubes[6].x = 2;layers[1][i].cubes[6].y = i;layers[1][i].cubes[6].z = 0;
		layers[1][i].cubes[7].x = 1;layers[1][i].cubes[7].y = i;layers[1][i].cubes[7].z = 0;
		layers[1][i].cubes[8].x = 1;layers[1][i].cubes[8].y = i;layers[1][i].cubes[8].z = 1;
		layers[1][i].axis = Vector3D(0.0, 1.0, 0.0);
	}
	//Construct layers rotate around z-axis
	for (int i = 0;i < 3;++i)
	{
		layers[2][i].cubes[0].x = 0;layers[2][i].cubes[0].y = 0;layers[2][i].cubes[0].z = i;
		layers[2][i].cubes[1].x = 1;layers[2][i].cubes[1].y = 0;layers[2][i].cubes[1].z = i;
		layers[2][i].cubes[2].x = 2;layers[2][i].cubes[2].y = 0;layers[2][i].cubes[2].z = i;
		layers[2][i].cubes[3].x = 2;layers[2][i].cubes[3].y = 1;layers[2][i].cubes[3].z = i;
		layers[2][i].cubes[4].x = 2;layers[2][i].cubes[4].y = 2;layers[2][i].cubes[4].z = i;
		layers[2][i].cubes[5].x = 1;layers[2][i].cubes[5].y = 2;layers[2][i].cubes[5].z = i;
		layers[2][i].cubes[6].x = 0;layers[2][i].cubes[6].y = 2;layers[2][i].cubes[6].z = i;
		layers[2][i].cubes[7].x = 0;layers[2][i].cubes[7].y = 1;layers[2][i].cubes[7].z = i;
		layers[2][i].cubes[8].x = 1;layers[2][i].cubes[8].y = 1;layers[2][i].cubes[8].z = i;
		layers[2][i].axis = Vector3D(0.0, 0.0, 1.0);
	}
}

RubikCube::~RubikCube()
{

}

/************************************************************************/
/* Query which layer of the Rubik's cube should rotate
@param select: the sub-cubes selected (represented by the integer-coordinates);
@param layerX, layerY: layer-coordinates
@param dir: rotating direction of the layer (1 or -1)*/
/************************************************************************/

bool RubikCube::QueryLayer(const std::vector<TriInt>& select, int& layerX, int& layerY, int& dir)
{
	/*
	TODO: you need to implement this function to judge which layer is selected and which direction it follows to rotate
	This function will decide how the program react to the operation on the Rubik's cube.
	*/
	return false;
}

/************************************************************************/
/* Rotate a single angle (rotateSpeed) for the sub-cubes in the given layer and store the result model-view matrix for each 
rotated sub-cube in its matrix variable
@param x, y: integer-coordinates of the rotated layer;
@param dir: direction of the rotation, 1 or -1.*/
/************************************************************************/
void RubikCube::RotateStep(int x, int y, int dir)
{
	//TODO:
}

/************************************************************************/
/* For the sub-cubes of rotated layers, update their colors.
@param x, y: integer-coordinates of the rotated layer
@param dir: rotating direction

This function focuses on two things:
1. Set the correct color for the rotated sub-cubes. Note that the correct color for 
each rotated sub-cubes can be calculated by some sub-cube in the same layer 
(mathematically this procedure can be called as PERMUTATION).
2. Reset matrix for each sub-cube to be identity. The matrix of each sub-cube is only 
used to display an animation of rotation. And only the rotated sub-cubes need to be 
reset to identity (others do not change).

If you want to apply texture on the Rubik's cube, this is the function you should work 
on.
*/
/************************************************************************/
void RubikCube::RotateFinish(int x, int y, int dir)
{
	//TODO:
}

void RubikCube::Render(GLenum mode)
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMultMatrixd(trackBall.MVmatrix);

	for (int i = 0;i < 3;++i)
	{
		for (int j = 0;j < 3;++j)
		{
			for (int k = 0;k < 3;++k)
			{
				if(mode == GL_FILL) glLoadName(i+j*3+k*9);
				RenderSingleCube(mode, i, j, k);
			}
		}
	}
	glPopMatrix();
}

void RubikCube::RenderSingleCube(GLenum mode, int x, int y, int z)
{
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMultMatrixd(magicCube[x][y][z].matrix);

	//move the whole Rubik's cube to the center
	glTranslatef(-3*cubeSize/2, -3*cubeSize/2, -3*cubeSize/2);

	if (mode == GL_LINE)
	{
		GLfloat sizes[2];
		GLfloat step;
		glGetFloatv(GL_LINE_WIDTH_RANGE, sizes);
		glGetFloatv(GL_LINE_WIDTH_GRANULARITY, &step);
		glLineWidth(sizes[0] + 40 * step);
		glEnable(GL_LINE_SMOOTH);
		glColor3ub(0, 0, 0);
	}

	glBegin(GL_QUADS);
	//front face
	if(mode == GL_FILL && hiddenFace[x][y][z][1])
		glColor3ub(magicCube[x][y][z].faceColor[1].x, magicCube[x][y][z].faceColor[1].y, magicCube[x][y][z].faceColor[1].z);
	else
		glColor3ub(0, 0, 0);

	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);

	//back face
	if(mode == GL_FILL && hiddenFace[x][y][z][3])
		glColor3ub(magicCube[x][y][z].faceColor[3].x, magicCube[x][y][z].faceColor[3].y, magicCube[x][y][z].faceColor[3].z);
	else
		glColor3ub(0, 0, 0);

	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);

	//top face
	if(mode == GL_FILL && hiddenFace[x][y][z][4])
		glColor3ub(magicCube[x][y][z].faceColor[4].x, magicCube[x][y][z].faceColor[4].y, magicCube[x][y][z].faceColor[4].z);
	else
		glColor3ub(0, 0, 0);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);

	//bottom face
	if(mode == GL_FILL && hiddenFace[x][y][z][5])
		glColor3ub(magicCube[x][y][z].faceColor[5].x, magicCube[x][y][z].faceColor[5].y, magicCube[x][y][z].faceColor[5].z);
	else
		glColor3ub(0, 0, 0);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);

	//right face
	if(mode == GL_FILL && hiddenFace[x][y][z][2])
		glColor3ub(magicCube[x][y][z].faceColor[2].x, magicCube[x][y][z].faceColor[2].y, magicCube[x][y][z].faceColor[2].z);
	else
		glColor3ub(0, 0, 0);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] + cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);

	//left face
	if(mode == GL_FILL && hiddenFace[x][y][z][0])
		glColor3ub(magicCube[x][y][z].faceColor[0].x, magicCube[x][y][z].faceColor[0].y, magicCube[x][y][z].faceColor[0].z);
	else
		glColor3ub(0, 0, 0);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] + cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] + cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glVertex3f(magicCube[x][y][z].pos[0] - cubeSize/2, magicCube[x][y][z].pos[1] - cubeSize/2, magicCube[x][y][z].pos[2] - cubeSize/2);
	glEnd();

	glPopMatrix();
}

/************************************************************************/
/*  Replace cube1's color with cube2's color under two permutations
@param cube1, cube2: two sub-cubes, former is destination, latter is source;
@param order, relativeTop: two permutations*/
/************************************************************************/
void RubikCube::ReplaceColors(Cube *cube1, Cube *cube2, int order[], int relativeTop[])
{
	cube1->faceColor[order[0]] = cube2->faceColor[order[1]];
	cube1->faceColor[order[1]] = cube2->faceColor[order[2]];
	cube1->faceColor[order[2]] = cube2->faceColor[order[3]];
	cube1->faceColor[order[3]] = cube2->faceColor[order[0]];
	cube1->faceColor[relativeTop[0]] = cube2->faceColor[relativeTop[0]];
	cube1->faceColor[relativeTop[1]] = cube2->faceColor[relativeTop[1]];
}