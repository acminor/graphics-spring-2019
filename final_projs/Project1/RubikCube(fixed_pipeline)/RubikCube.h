#ifndef _RUBIKCUBE_H_
#define _RUBIKCUBE_H_

#include <vector>
#include <list>
#include "Vector3D.h"
#include <gl\glut.h>
#include "trackball.h"
#include <ctime>

//sub-cube size
const double cubeSize = 1.0f;

struct TriInt
{
	int x, y, z;
};

//Sub-cube structure
struct Cube
{
	//colors of faces
	TriInt faceColor[6];

	//center of the sub-cube
	Vector3D pos;

	//model-view matrix, used to display an animation
	double matrix[16];
};

//Layer structure
struct Layer
{
	//sub-cubes. represented by integer-coordinates
	//see the constructor of RubikCube for details
	TriInt cubes[9];

	//rotating axis
	Vector3D axis;
};

//Structure of Rubik's cube
class RubikCube
{
	//27 sub-cubes, organized as a 3 by 3 by 3 structure.
	Cube magicCube[3][3][3];

	//9 layers, organized as a 3 by 3 as the rotating axis and position.
	Layer layers[3][3];

public:
	//trackball, controlling the 3D position of the whole Rubik's cube.
	TrackBall trackBall;

public:
	RubikCube();
	~RubikCube();

	bool QueryLayer(const std::vector<TriInt>& select, int& layerX, int& layerY, int& dir);

	void RotateStep(int x, int y, int dir);

	void RotateFinish(int x, int y, int dir);

	//Render
	void Render(GLenum mode);

private:

	void RenderSingleCube(GLenum mode, int x, int y, int z);

	void ReplaceColors(Cube *cube1, Cube *cube2, int order[], int relativeTop[]);
};

#endif