#version 330 core
layout (location = 0) in vec3 position;

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform sampler2D heightmap;
uniform bool flip;

out vec3 pos;
out float height_out;

void main()
{
  //
  float scale = 1.2f;
  float height = texture2D(heightmap,
                           vec2(position.x,
                                position.y)).r;
  float scale_wd = 2.0f;
  height = scale * height;
  height_out = height;
  if (flip) {
    if (height < .2)
      height = .2;
    height = -height;
    gl_Position = projection * view * model * vec4(scale_wd*position.x,
                                                   .4*height + 0.24,
                                                   scale_wd*position.y,
                                                   1.0f);
  } else {
    gl_Position = projection * view * model * vec4(scale_wd*position.x,
                                                   .4*height+0.01,
                                                   scale_wd*position.y,
                                                   1.0f);
  }

  pos = position;
}
