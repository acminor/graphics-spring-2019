#version 330 core
layout (location = 0) in vec3 position;
layout (location = 1) in vec2 texp;

uniform bool flip;
uniform int frame_shift;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform sampler2D heightmap;

out vec3 pos;

void main()
{
  float py;
  if (flip) {
    py = 1+position.y;
  } else {
    py = -.9-position.y;
  }

  float scale = 5.0f;
  gl_Position = projection * view * model * vec4(scale*position.x,
                                                 scale*py + .028,
                                                 scale*position.z,
                                                 1.0f);
  pos = vec3(position.xyz);
}
