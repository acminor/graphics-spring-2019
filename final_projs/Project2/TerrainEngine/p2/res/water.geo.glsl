#version 330 core

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec3 pos[];

out vec3 posOut;
out vec3 Normal;
out vec3 NormalPos;

void main(void)
{
  vec4 PositionIn[3] = vec4[]((gl_in[0].gl_Position),
                              (gl_in[1].gl_Position),
                              (gl_in[2].gl_Position));
  // Normal calculation code from
  // https://www.gamedev.net/forums/topic/552864-recalculating-normals-in-geometry-shader/
  posOut = vec3(pos[0]);
  Normal = normalize(cross(PositionIn[1].xyz - PositionIn[0].xyz,
                           PositionIn[2].xyz - PositionIn[0].xyz)).xyz;
  NormalPos = vec3(PositionIn[0]);
  gl_Position = PositionIn[0];
  EmitVertex();

  Normal = normalize(cross(PositionIn[2].xyz - PositionIn[1].xyz,
                           PositionIn[0].xyz - PositionIn[1].xyz)).xyz;
  posOut = vec3(pos[1]);
  NormalPos = vec3(PositionIn[1]);
  gl_Position = PositionIn[1];
  EmitVertex();

  Normal = normalize(cross(PositionIn[0].xyz - PositionIn[2].xyz,
                           PositionIn[1].xyz - PositionIn[2].xyz)).xyz;
  posOut = vec3(pos[2]);
  NormalPos = vec3(PositionIn[2]);
  gl_Position = PositionIn[2];
  EmitVertex();

  EndPrimitive();

  for(int i = 0; i < 3; i++) { // You used triangles, so it's always 3
    gl_Position = gl_in[i].gl_Position;
    EmitVertex();
  }
  EndPrimitive();
}
