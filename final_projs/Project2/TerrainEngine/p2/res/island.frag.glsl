#version 330 core

uniform float max_w;
uniform float min_w;
uniform sampler2D tex;
uniform sampler2D tex1;
in vec3 pos;
in float height_out;

vec4 clear = vec4(0.0,0.0,0.0,0.0);
float cut_pos = .1;

void main()
{
  float pos_scale = 10.0f;
  float pos_x = pos_scale*pos.x;
  float pos_y = pos_scale*pos.y;
  vec4 c1 = texture2D(tex,
                      vec2((pos.x),
                           (pos.y)));
  vec4 c2 = texture2D(tex1,
                      vec2((pos_x),
                           (pos_y)));

  if ((pos.x < cut_pos || pos.x > (1-cut_pos) ||
       pos.y < cut_pos || pos.y > (1-cut_pos)) &&
      height_out < .299f) {
    gl_FragColor = clear;
  } else {
    gl_FragColor = mix(c1, c2, .6);
  }
}
