#version 330 core

uniform float max_w;
uniform float min_w;
uniform samplerCube cmap;
in vec3 pos;

void main()
{
  vec3 pos1 = vec3(pos.x, -pos.y, pos.z);
  gl_FragColor = texture(cmap, pos1);
  // useless
}
