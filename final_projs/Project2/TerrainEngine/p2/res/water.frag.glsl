#version 330 core

uniform float max_w;
uniform float min_w;
uniform sampler2D tex;
uniform sampler2D tex1;
uniform samplerCube cmap;
uniform vec3 cameraPos;
in vec3 posOut;
in vec3 Normal;
in vec3 NormalPos;

void main()
{
  float pos_scale = 1.0f;
  float pos_x = pos_scale*posOut.x;
  float pos_y = pos_scale*posOut.y;
  vec3 I = normalize(NormalPos);
  vec3 R = -reflect(I, normalize(Normal));
  vec4 color = texture2D(tex,
                         vec2((posOut.x),
                              (posOut.y)));
  /*vec4 c2 = texture2D(tex1,
                      vec2((pos_x),
                           (pos_y)));
  */
  //vec4 color = mix(c1, c2, .4);
  //gl_FragColor = vec4(color.rgb, .6);
  //  gl_FragColor = vec4(1.0,1.0,1.0,1.0);
  vec4 c1 = vec4(texture(cmap, R).rgb, 1.0);
  vec4 t = mix(color, c1, .03);
  gl_FragColor = vec4(t.rgb, .75);
  // useless
}
