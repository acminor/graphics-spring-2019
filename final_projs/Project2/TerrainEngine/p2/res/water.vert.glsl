#version 330 core
layout (location = 0) in vec3 position;

uniform int frame_shift;
uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;
uniform sampler2D heightmap;
uniform sampler2D tex;

out vec3 pos;

void main()
{
  float scale_pos = 2.0; //
  float scale_fs = 0.1;
  float scale_tm = 0.002;
  float scale_ht = .007; //
  float scale_wd = 6.0f;
  float pos_x = position.x*scale_pos*scale_wd
    + scale_fs*cos(scale_tm*frame_shift)
    + scale_fs*sin(scale_tm*frame_shift);
  float pos_y = position.y*scale_pos*scale_wd
    + scale_fs*cos(2*scale_tm*frame_shift)
    + scale_fs*sin(scale_tm*frame_shift);
  vec3 temp = texture2D(tex,
                        vec2(pos_x,
                             pos_y)).rgb;
  float height = (temp.r + temp.g + temp.b)/3.0f;
  gl_Position = projection * view * model *
    vec4(scale_wd*position.x - 1.5,
         position.z+
         scale_wd*scale_ht*height - .017,
         scale_wd*position.y - 1.5,
         1.0f);
  pos = vec3(pos_x, pos_y, position.z*100);
}
