#include "config_shader.h"

void init_ConfigurableShader(ConfigurableShader* self, Shader* shader) {
  self->shader = shader;
  self->wireframe = false;
  self->points = false;
  self->faces = false;
  self->use = ConfigurableShader_use;
  self->texture = -1;
  self->heightmap = -1;
  self->frame_shift = 0;
  self->flip = false;
  self->cubemap = -1;

  glm_mat4_identity(self->model);
  glm_mat4_identity(self->view);
  glm_mat4_identity(self->projection);
  glm_mat4_identity(self->world_scale);
}

void new_ConfigurableShader(ConfigurableShader** this, Shader* shader) {
  (*this) = malloc(sizeof(ConfigurableShader));
  init_ConfigurableShader((*this), shader);
}

void ConfigurableShader_use(ConfigurableShader* self) {
  self->shader->use(self->shader);

  GLint modelLoc = glGetUniformLocation(self->shader->program,
                                        "model");
  GLint viewLoc = glGetUniformLocation(self->shader->program,
                                       "view");
  GLint projLoc = glGetUniformLocation(self->shader->program,
                                       "projection");
  GLint wfColorLoc = glGetUniformLocation(self->shader->program,
                                          "wireframe_color");
  GLint flipLoc = glGetUniformLocation(self->shader->program,
                                       "flip");
  GLint hmLoc = glGetUniformLocation(self->shader->program,
                                     "heightmap");
  GLint texLoc = glGetUniformLocation(self->shader->program,
                                      "tex");
  GLint tex1Loc = glGetUniformLocation(self->shader->program,
                                      "tex1");
  GLint cmapLoc = glGetUniformLocation(self->shader->program,
                                       "cmap");
  GLint fsLoc = glGetUniformLocation(self->shader->program,
                                     "frame_shift");
  GLint wsLoc = glGetUniformLocation(self->shader->program,
                                     "world_scale");

  glUniformMatrix4fv(modelLoc, 1, GL_FALSE, (GLfloat*) self->model);
  glUniformMatrix4fv(viewLoc, 1, GL_FALSE, (GLfloat*) self->view);
  glUniformMatrix4fv(projLoc, 1, GL_FALSE, (GLfloat*) self->projection);
  glUniformMatrix4fv(wsLoc, 1, GL_FALSE, (GLfloat*) self->world_scale);
  glUniform1i(wfColorLoc, self->wireframe);
  glUniform1i(flipLoc, self->flip);
  glUniform1i(fsLoc, self->frame_shift);

  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, self->texture);
  glUniform1i(texLoc, 0);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_2D, self->heightmap);
  glUniform1i(hmLoc, 1);
  glActiveTexture(GL_TEXTURE2);
  glBindTexture(GL_TEXTURE_2D, self->texture1);
  glUniform1i(tex1Loc, 2);
  glActiveTexture(GL_TEXTURE3);
  glBindTexture(GL_TEXTURE_CUBE_MAP, self->cubemap);
  glUniform1i(cmapLoc, 3);
}
