#include <cglm/cglm.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "array.h"
#include "object_reader.h"
#include "config_shader.h"
#include "util.h"

void parse_obj(GLchar* obj_file_path, Mesh* out) {
  if (out == NULL) {
    printf("FAILURE: PASSED NULL POINTER TO PARSE_OBJ");
    exit(1);
  }

  /* NOTE: probably should make client init mesh
     but we will do it here for now */
  out->free = Mesh_free;
  out->number_of_faces = 0;

  Array vtc;
  Array_init(&vtc);

  out->head = malloc(sizeof(Face));
  Face *current, *previous;
  current = previous = out->head;

  GLchar* sobj = NULL;
  FILE* obj_file = fopen(obj_file_path, "r");
  mn_read_file(&sobj, obj_file);
  fclose(obj_file);

  char c = sobj[0];
  int i = 0;
  while (c != '\0') {
    switch (c) {
      GLfloat x, y, z;
      GLchar buff[128];
      float* temp;
      GLuint p1, p2, p3;
    case '#':
      while (c != '\n' && c != '\0') {
                                      i++;
                                      c = sobj[i];
      }
      break;
    case 'v':
      i+=2; /* NOTE: assumes only one space */
      /* NOTE: does no error checking on object file */
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%f", &x);
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%f", &y);
      i+=mn_po_read_num_to_buff(buff, sobj+i);
      sscanf(buff, "%f", &z);

      //printf("Reading vertex: %f %f %f\n", x, y, z);

      temp = malloc(3*sizeof(float));
      temp[0] = x;
      temp[1] = y;
      temp[2] = z;
      Array_append(&vtc, (void*) temp);
      break;
    case 'f':
      i+=2; /* NOTE: assumes only one space */
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%i", &p1);
      i+=mn_po_read_num_to_buff(buff, sobj+i)+1;
      sscanf(buff, "%i", &p2);
      i+=mn_po_read_num_to_buff(buff, sobj+i);
      sscanf(buff, "%i", &p3);
      //      printf("Reading face: %d %d %d\n", p1, p2, p3);

      /* NOTE: obj vertices are indexed from 1 */
      Array_get(&vtc, p1-1, (void**) &temp);
      current->face[0][0] = temp[0];
      current->face[0][1] = temp[1];
      current->face[0][2] = temp[2];
      Array_get(&vtc, p2-1, (void**) &temp);
      current->face[1][0] = temp[0];
      current->face[1][1] = temp[1];
      current->face[1][2] = temp[2];
      Array_get(&vtc, p3-1, (void**) &temp);
      current->face[2][0] = temp[0];
      current->face[2][1] = temp[1];
      current->face[2][2] = temp[2];
      out->number_of_faces++;

      current->next = malloc(sizeof(Face));

      previous = current;
      current = current->next;
      break;
    default:
      i++;
      break;
    }

    /* Get the current character */
    c = sobj[i];
  }

  /* NOTE: we always allocate for the next read
     this leaves an extra node allocated on finish
     get rid of this node */
  previous->next = NULL;
  free(current);
}

void Mesh_free(Mesh* mesh) {
  Face* current = mesh->head;
  Face* next = NULL;
  while (current != NULL) {
    next = current->next;
    free(current);
    current = next;
  }
}
