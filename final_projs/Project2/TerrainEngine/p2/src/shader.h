#ifndef SHADER_H
#define SHADER_H

#include <stdio.h>
#include <stdlib.h>

#include <glad/glad.h>
#include "util.h"

struct Shader {
  GLuint program;
  void (*use)(struct Shader*);
} typedef Shader;

void init_Shader(Shader*, const GLchar*, const GLchar*, const GLchar*);
void new_Shader(Shader**, const GLchar*, const GLchar*, const GLchar*);
void shader_use(Shader*);

#endif /* SHADER_H */
