#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "config_shader.h"
#include "skymap.h"
#include "object.h"
#include "object_reader.h"
#include "util.h"
#include "camera.h"

#define WIDTH 1280
#define HEIGHT 800

void init_SkyMap(Object* container,
                           SkyMapArgs args) {
  SkyMap *self = malloc(sizeof(SkyMap));
  // TODO CITE
  float skyboxVertices[] = {
                            // positions
                            -1.0f,  1.0f, -1.0f,
                            -1.0f, -1.0f, -1.0f,
                            1.0f, -1.0f, -1.0f,
                            1.0f, -1.0f, -1.0f,
                            1.0f,  1.0f, -1.0f,
                            -1.0f,  1.0f, -1.0f,

                            -1.0f, -1.0f,  1.0f,
                            -1.0f, -1.0f, -1.0f,
                            -1.0f,  1.0f, -1.0f,
                            -1.0f,  1.0f, -1.0f,
                            -1.0f,  1.0f,  1.0f,
                            -1.0f, -1.0f,  1.0f,

                            1.0f, -1.0f, -1.0f,
                            1.0f, -1.0f,  1.0f,
                            1.0f,  1.0f,  1.0f,
                            1.0f,  1.0f,  1.0f,
                            1.0f,  1.0f, -1.0f,
                            1.0f, -1.0f, -1.0f,

                            -1.0f, -1.0f,  1.0f,
                            -1.0f,  1.0f,  1.0f,
                            1.0f,  1.0f,  1.0f,
                            1.0f,  1.0f,  1.0f,
                            1.0f, -1.0f,  1.0f,
                            -1.0f, -1.0f,  1.0f,

                            -1.0f,  1.0f, -1.0f,
                            1.0f,  1.0f, -1.0f,
                            1.0f,  1.0f,  1.0f,
                            1.0f,  1.0f,  1.0f,
                            -1.0f,  1.0f,  1.0f,
                            -1.0f,  1.0f, -1.0f,

                            -1.0f, -1.0f, -1.0f,
                            -1.0f, -1.0f,  1.0f,
                            1.0f, -1.0f, -1.0f,
                            1.0f, -1.0f, -1.0f,
                            -1.0f, -1.0f,  1.0f,
                            1.0f, -1.0f,  1.0f
  };

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices),
               skyboxVertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  self->shader = args.shader;
  self->current_frame = 0;
  self->render_step = false;

  glGenTextures(1, &self->cubemap);
  glBindTexture(GL_TEXTURE_CUBE_MAP, self->cubemap);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  self->shader->cubemap = self->cubemap;

  {
    int width, height;
    unsigned char* image =
      SOIL_load_image(args.right, &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 0, 0, GL_RGBA, width, height,
                0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }
  {
    int width, height;
    unsigned char* image =
      SOIL_load_image(args.left, &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 1, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }
  {
    int width, height;
    unsigned char* image =
      SOIL_load_image("res/clear.png", &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 2, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }
  {
    int width, height;
    unsigned char* image =
      SOIL_load_image(args.top, &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 3, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }
  {
    int width, height;
    unsigned char* image =
      SOIL_load_image(args.front, &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    // skip 3 as that is bottom
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 4, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }
  {
    int width, height;
    unsigned char* image =
      SOIL_load_image(args.back, &width, &height,
                      0, SOIL_LOAD_RGBA);

    // load and generate the texture
    glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + 5, 0, GL_RGBA, width, height,
                 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
    glGenerateMipmap(GL_TEXTURE_CUBE_MAP);

    //unbind the texture and free memory
    SOIL_free_image_data(image);
  }

  glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

  IODesc obj_desc = {
    .free_addr=SkyMap_free,
    .render_addr=SkyMap_render,
    .update_addr=SkyMap_update
  };

  init_Object(container, (void*) self, obj_desc);

  printf("Object:\n");
  printf("-NumFaces %d\n", self->number_of_faces);

  // NOTE positions object in scene
  //glm_rotate(self->shader->model, glm_rad(45.0), (vec3){1.0, 0.0, 0.0});

}

void new_SkyMap(Object** container,
                          SkyMapArgs args) {
  (*container) = malloc(sizeof(Object));
  init_SkyMap((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {                \
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void SkyMap_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  SkyMap *self = (SkyMap*) container->object;
}

void SkyMap_render(Object* container, Camera* camera) {
  SkyMap *self = (SkyMap*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);
  glBindVertexArray(self->vao);

  if (self->render_step) {
    self->shader->flip = true;
    self->render_step ^= true;
  }
  else {
    self->shader->flip = false;
    self->render_step ^= true;
  }

  mat4 scalem, translatem, rotm, rotm2, assignm;
  float scale = 4.0f;
  glm_mat4_identity(assignm);
  glm_scale_uni(assignm, scale);
  //glm_scale_make(scalem, (vec3){scale, scale, scale});
  //glm_rotate_make(rotm, glm_rad(45), (vec3){0.0,0.0,1.0});
  //glm_rotate_make(rotm, glm_rad(90), (vec3){0.0,0.0,1.0});
  //glm_rotate_make(rotm2, glm_rad(270), (vec3){1.0,0.0,0.0});
  //glm_translate_make(translatem, (vec3){-0.5f, -0.5f, 1.0f});
  //glm_mat4_mulN((mat4 *[]){&scalem, &rotm2, &rotm, &translatem}, 4,
  //              assignm);
  glm_mat4_copy(assignm, self->shader->model);

  self->shader->cubemap = self->cubemap;
  self->shader->use(self->shader);
  glDrawArrays(GL_TRIANGLES, 0, 36);

  glBindVertexArray(0);
}

void SkyMap_free(Object* container) {
  SkyMap *self = (SkyMap*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
