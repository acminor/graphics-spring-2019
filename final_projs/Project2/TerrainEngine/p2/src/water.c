#include <cglm/cglm.h>
#include <glad/glad.h>
#include <stdbool.h>
#include <GLFW/glfw3.h>
#include <SOIL/SOIL.h>
#include <string.h>
#include <time.h>
#include <math.h>

#include "config_shader.h"
#include "water.h"
#include "object.h"
#include "object_reader.h"
#include "planet_reader.h"
#include "util.h"
#include "camera.h"

#define WIDTH 1280
#define HEIGHT 800

void init_Water(Object* container,
                           WaterArgs args) {
  Water *self = malloc(sizeof(Water));
  Mesh obj;
  parse_obj(args.obj_file_path, &obj);

  GLuint vertices_size = obj.number_of_faces*9*sizeof(GLfloat);
  GLfloat* vertices = malloc(vertices_size);
  Face* cur = obj.head;
  int vpos = 0;
  while (cur != NULL) {
    // NOTE: we will use texture mapping lat-long provided
    // here https://en.wikibooks.org/wiki/GLSL_Programming/GLUT/Textured_Spheres
    vertices[vpos]    = cur->face[0][0];
    vertices[vpos+1]  = cur->face[0][1];
    vertices[vpos+2]  = cur->face[0][2];
    vertices[vpos+3]  = cur->face[1][0];
    vertices[vpos+4]  = cur->face[1][1];
    vertices[vpos+5]  = cur->face[1][2];
    vertices[vpos+6]  = cur->face[2][0];
    vertices[vpos+7]  = cur->face[2][1];
    vertices[vpos+8]  = cur->face[2][2];

    vpos += 9;
    cur = cur->next;
  }

  glGenVertexArrays(1, &self->vao);
  glGenBuffers(1, &self->vbo);
  glBindVertexArray(self->vao);
  glBindBuffer(GL_ARRAY_BUFFER, self->vbo);
  glBufferData(GL_ARRAY_BUFFER, vertices_size,
               vertices, GL_STATIC_DRAW);

  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
                        3*sizeof(GLfloat), (GLvoid*) 0);
  glEnableVertexAttribArray(0);

  glBindVertexArray(0); /* unbind vao */

  self->number_of_faces = obj.number_of_faces;
  self->shader = args.shader;
  self->current_frame = 0;
  self->cubemap = args.cubemap;
  self->shader->cubemap = args.cubemap;

  mat4 scalem, translatem;
  glm_scale_make(scalem, (vec3){args.scale, args.scale, args.scale});
  glm_translate_make(translatem, args.offset);
  glm_mat4_mulN((mat4 *[]){&translatem, &scalem}, 2, self->shader->model);

  int width, height;
  unsigned char* image =
    SOIL_load_image(args.texture1, &width, &height,
                    0, SOIL_LOAD_RGBA);

  glGenTextures(1, &self->shader->texture);
  glBindTexture(GL_TEXTURE_2D, self->shader->texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_MIRRORED_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  // load and generate the texture
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height,
               0, GL_RGBA, GL_UNSIGNED_BYTE, image);
  glGenerateMipmap(GL_TEXTURE_2D);

  //unbind the texture and free memory
  SOIL_free_image_data(image);
  glBindTexture(GL_TEXTURE_2D, 0);

  IODesc obj_desc = {
    .free_addr=Water_free,
    .render_addr=Water_render,
    .update_addr=Water_update
  };

  init_Object(container, (void*) self, obj_desc);

  printf("Object:\n");
  printf("-NumFaces %d\n", self->number_of_faces);

  // NOTE positions object in scene
  //glm_rotate(self->shader->model, glm_rad(45.0), (vec3){1.0, 0.0, 0.0});

  obj.free(&obj);
  free(vertices);
}

void new_Water(Object** container,
                          WaterArgs args) {
  (*container) = malloc(sizeof(Object));
  init_Water((*container), args);
}

#define DEBOUNCE(METHOD, KEY) {                \
    static clock_t KEY##time; \
    if (abs(KEY##time - clock())/(float)CLOCKS_PER_SEC > .012) { \
        KEY##time = clock(); \
        METHOD;\
    }}

#define UPDATE_CASE(STATE,KEY)                    \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void Water_update(Object* container, SceneState* state) {
  InputState is = state->input_state;
  Water *self = (Water*) container->object;

  self->current_frame++;
  self->shader->frame_shift = self->current_frame;
}

void Water_render(Object* container, Camera* camera) {
  Water *self = (Water*) container->object;
  glm_mat4_copy(camera->view, self->shader->view);
  glm_mat4_copy(camera->projection, self->shader->projection);

  glBindTexture(GL_TEXTURE_2D, self->shader->texture1);
  glGenerateMipmap(GL_TEXTURE_2D);
  glBindTexture(GL_TEXTURE_2D, 0);

  self->shader->use(self->shader);
  glBindVertexArray(self->vao);
  glDrawArrays(GL_TRIANGLES, 0, 3*self->number_of_faces);
  glBindVertexArray(0);
}

void Water_free(Object* container) {
  Water *self = (Water*) container->object;
  glDeleteVertexArrays(1, &self->vao);
  glDeleteBuffers(1, &self->vbo);
}
