#include "camera.h"
#include "scene.h"

void init_Camera(Camera* self) {
  glm_mat4_identity(self->view);
  glm_mat4_identity(self->projection);

  glm_vec3_copy((vec3){0.0f,2.0f,8.0f}, self->position);
  glm_vec3_copy((vec3){0.0f,0.0f,-1.0f}, self->front);
  glm_vec3_copy((vec3){0.0f,1.0f,0.0f}, self->up);
  glm_vec3_copy((vec3){0.0f,0.0f,0.0f}, self->right);
  glm_vec3_copy(self->up, self->world_up);

  self->yaw = YAW;
  self->pitch = PITCH;
  self->movement_speed = SPEED;
  self->mouse_sensitivity = SENSITIVITY;
  self->zoom = ZOOM;

  Camera_update_vectors(self);
  Camera_update_projection(self);
}

void new_Camera(Camera** self) {
  (*self) = malloc(sizeof(Camera));
  init_Camera(*self);
}

void Camera_update_vectors(Camera* self) {
  vec3 front;
  front[X] = cos(glm_rad(self->yaw)) * cos(glm_rad(self->pitch));
  front[Y] = sin(glm_rad(self->pitch));
  front[Z] = sin(glm_rad(self->yaw)) * cos(glm_rad(self->pitch));
  glm_vec3_normalize(front);
  glm_vec3_copy(front, self->front);

  vec3 right;
  glm_vec3_cross(self->front, self->world_up, right);
  glm_vec3_normalize(right);
  glm_vec3_copy(right, self->right);

  vec3 up;
  glm_vec3_cross(self->right, self->front, up);
  glm_vec3_normalize(up);
  glm_vec3_copy(up, self->up);

  vec3 center;
  glm_vec3_add(self->position, self->front, center);
  glm_lookat(self->position, center, self->up, self->view);
}

void Camera_update_projection(Camera* self) {
  glm_perspective(glm_rad(self->zoom), (float)WIDTH_/(float)HEIGHT_,
                  0.1f, 1000.0f, self->projection);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
void _Camera_update_keys(Camera* self, SceneState state) {
  InputState is = state.input_state;

  vec3 front;
  glm_vec3_copy(self->front, front);
  glm_vec3_scale(front, self->movement_speed, front);

  vec3 right;
  glm_vec3_copy(self->right, right);
  glm_vec3_scale(right, self->movement_speed, right);

  vec3 up;
  glm_vec3_copy(self->up, up);
  glm_vec3_scale(up, self->movement_speed, up);

  UPDATE_CASE(is, key_w)
    glm_vec3_add(self->position, front, self->position);
  UPDATE_CASE(is, key_s)
    glm_vec3_sub(self->position, front, self->position);
  UPDATE_CASE(is, key_a)
    glm_vec3_sub(self->position, right, self->position);
  UPDATE_CASE(is, key_d)
    glm_vec3_add(self->position, right, self->position);
  UPDATE_CASE(is, key_q)
    glm_vec3_add(self->position, up, self->position);
  UPDATE_CASE(is, key_e)
    glm_vec3_sub(self->position, up, self->position);

  if (self->position[Y] < .7f)
    self->position[Y] = .7f;
  if (self->position[X] > 10.0f)
    self->position[X] = 10.0f;
  if (self->position[X] < -10.0f)
    self->position[X] = -10.0f;
  if (self->position[Z] > 10.0f)
    self->position[Z] = 10.0f;
  if (self->position[Z] < -10.0f)
    self->position[Z] = -10.0f;

}

void _Camera_update_mouse(Camera* self, SceneState state) {
  InputState is = state.input_state;
  float xoffset = is.xoffset;
  float yoffset = is.yoffset;

  xoffset *= self->mouse_sensitivity;
  yoffset *= self->mouse_sensitivity;

  self->yaw += xoffset;
  self->pitch += yoffset;

  if (self->pitch > 89.0f)
    self->pitch = 89.0f;
  if (self->pitch < -89.0f)
    self->pitch = -89.0f;

  Camera_update_vectors(self);
}

void _Camera_update_scroll(Camera* self, SceneState state) {
  InputState is = state.input_state;

  if (self->zoom >= 1.0f && self->zoom <= 45.0f)
    self->zoom -= is.scroll_offset;
  if (self->zoom <= 1.0f)
    self->zoom = 1.0f;
  if (self->zoom >= 45.0f)
    self->zoom = 45.0f;

  Camera_update_projection(self);
}

void Camera_update(Camera* self, SceneState is) {
  _Camera_update_keys(self, is);
  _Camera_update_mouse(self, is);
  _Camera_update_scroll(self, is);
}
