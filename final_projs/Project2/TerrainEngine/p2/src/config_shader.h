#ifndef CONFIG_SHADER_H
#define CONFIG_SHADER_H

#include <cglm/cglm.h>
#include <stdbool.h>

#include "shader.h"

struct ConfigurableShader {
  Shader* shader;
  mat4 model;
  mat4 view;
  mat4 projection;
  mat4 world_scale;
  int texture;
  int texture1;
  int cubemap;
  int heightmap;
  int frame_shift;
  int max;
  int min;
  bool wireframe;
  bool points;
  bool faces;
  bool flip;
  void (*use)(struct ConfigurableShader*);
} typedef ConfigurableShader;

void ConfigurableShader_use(ConfigurableShader*);
void init_ConfigurableShader(ConfigurableShader*, Shader*);
void new_ConfigurableShader(ConfigurableShader**, Shader*);

#endif /* CONFIG_SHADER_H */
