#ifndef ISLAND_H
#define ISLAND_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Island;
struct IslandArgs;

void init_Island(struct Object*,
                           struct IslandArgs);
void new_Island(struct Object**,
                          struct IslandArgs);
void Island_render(struct Object*, Camera*);
void Island_free(struct Object*);
void Island_update(Object*, SceneState*);

struct IslandArgs {
  GLchar* obj_file_path;
  GLchar* heightmap;
  GLchar* texture1;
  GLchar* texture2;
  ConfigurableShader* shader;
} typedef IslandArgs;

struct Island {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  ConfigurableShader* shader;
  unsigned int current_frame;
  bool render_step;
} typedef Island;

#endif /* ISLAND_H */
