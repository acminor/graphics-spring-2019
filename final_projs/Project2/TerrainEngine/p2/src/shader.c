#include <cglm/cglm.h>
#include "shader.h"

void new_Shader(Shader** this,
                const GLchar* vertexPath,
                const GLchar* fragmentPath,
                const GLchar* geometryPath) {
  (*this) = malloc(sizeof(Shader));
  init_Shader((*this), vertexPath, fragmentPath, geometryPath);
}

void init_Shader(Shader* this,
                 const GLchar* vertexPath,
                 const GLchar* fragmentPath,
                 const GLchar* geometryPath) {
  FILE *vShaderFile, *fShaderFile, *gShaderFile;
  GLchar *vShaderCode, *fShaderCode, *gShaderCode;
  GLuint vertex, fragment, geometry;
  GLint status;
  GLchar infoLog[512];

  // 1. Retrieve the vertex/fragment source code from filePath
  vShaderFile = fopen(vertexPath, "r");
  fShaderFile = fopen(fragmentPath, "r");
  if (geometryPath != NULL)
    gShaderFile = fopen(geometryPath, "r");
  status = (vShaderFile != 0) && (fShaderFile != 0);
	if (!status) {
    printf("ERROR::SHADER::FILE_NOT_SUCCESSFULLY_READ\n");
    exit(-1);
  }
  mn_read_file(&vShaderCode, vShaderFile);
  mn_read_file(&fShaderCode, fShaderFile);
  if (geometryPath != NULL)
    mn_read_file(&gShaderCode, gShaderFile);
  // close file handlers
  fclose(vShaderFile);
  fclose(fShaderFile);
  if (geometryPath != NULL)
    fclose(gShaderFile);
  // 2. Compile shaders
  // Vertex Shader
  vertex = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertex, 1, (const GLchar**)&vShaderCode, NULL);
  glCompileShader(vertex);
  // Print compile errors if any
  glGetShaderiv(vertex, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(vertex, 512, NULL, infoLog);
    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
  }
  // Fragment Shader
  fragment = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragment, 1, (const GLchar**)&fShaderCode, NULL);
  glCompileShader(fragment);
  // Print compile errors if any
  glGetShaderiv(fragment, GL_COMPILE_STATUS, &status);
  if (!status) {
    glGetShaderInfoLog(fragment, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Geometry Shader
  if (geometryPath != NULL) {
    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, (const GLchar**)&gShaderCode, NULL);
    glCompileShader(geometry);
    // Print compile errors if any
    glGetShaderiv(geometry, GL_COMPILE_STATUS, &status);
    if (!status) {
      glGetShaderInfoLog(geometry, 512, NULL, infoLog);
      printf("ERROR::SHADER::GEOMETRY::COMPILATION_FAILED\n%s\n", infoLog);
      exit(-1);
    }
  }
  // Shader Program
  this->program = glCreateProgram();
  glAttachShader(this->program, vertex);
  glAttachShader(this->program, fragment);
  if (geometryPath != NULL)
    glAttachShader(this->program, geometry);
  glLinkProgram(this->program);
  // Print linking errors if any
  if ((glGetProgramiv(this->program, GL_LINK_STATUS, &status), !status)) {
    glGetProgramInfoLog(this->program, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n%s\n", infoLog);
    exit(-1);
	}
  // Delete the shaders as they're linked into our program now and no longer necessary
  glDeleteShader(vertex);
  glDeleteShader(fragment);
  if (geometryPath != NULL)
    glDeleteShader(geometry);

  /* Attach functions to our shader object */
  this->use = shader_use;

  /* free temporary buffers and variables */
  free(vShaderCode);
  free(fShaderCode);
  if (geometryPath != NULL)
    free(gShaderCode);
}

void shader_use(Shader* shader) {
  glUseProgram(shader->program);
}
