#ifndef WATER_H
#define WATER_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct Water;
struct WaterArgs;

void init_Water(struct Object*,
                 struct WaterArgs);
void new_Water(struct Object**,
                struct WaterArgs);
void Water_render(struct Object*, Camera*);
void Water_free(struct Object*);
void Water_update(Object*, SceneState*);

struct WaterArgs {
  GLchar* obj_file_path;
  GLchar* texture1;
  GLfloat scale;
  GLfloat* offset;
  ConfigurableShader* shader;
  GLuint cubemap;
} typedef WaterArgs;

struct Water {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  GLuint cubemap;
  ConfigurableShader* shader;
  unsigned int current_frame;
} typedef Water;

#endif /* WATER_H */
