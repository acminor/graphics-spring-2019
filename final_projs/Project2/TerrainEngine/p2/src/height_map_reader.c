#include <SOIL/SOIL.h>
#include "height_map_reader.h"
#include "array.h"
#include "object_reader.h"

struct Pair {
  int x;
  int y;
} typedef IntPair;

struct Point {
  float x;
  float y;
  float z;
} typedef Point;

struct Quad {
  Point tl;
  Point tr;
  Point bl;
  Point br;
} typedef Quad;

void hmap_to_mesh(char* hmap_path, Mesh* mesh,
                  htbargs args) {
  IntPair hw;
  unsigned char* hmap = SOIL_load_image(hmap_path, &hw.x, &hw.y,
                                        0, SOIL_LOAD_RGB);

  Array quads;
  Quad* quad;
  Array_init(&quads);

  float scale = 1;
  for (int i = 0; i < hw.x - 1; i++) {
    for (int j = 0; j < hw.y - 1; j++) {
      quad = malloc(sizeof(Quad));
      quad->tl.x = i;
      quad->tl.y = j;
      quad->tl.z = scale*(hmap[i + j*hw.x]);

      quad->tr.x = i+1;
      quad->tr.y = j;
      quad->tr.z = scale*(hmap[(i+1) + j*hw.x]);

      quad->bl.x = i;
      quad->bl.y = j+1;
      quad->bl.z = scale*(hmap[i + (j+1)*hw.x]);

      quad->br.x = i+1;
      quad->br.y = j+1;
      quad->br.z = scale*(hmap[(i+1) + (j+1)*hw.x]);

      Array_append(&quads, (void*) quad);
    }
  }

  Face* face;
  mesh->number_of_faces = 0;
  mesh->free = Mesh_free;
  mesh->head = malloc(sizeof(Face));
  face = mesh->head;
  for (int i = 0; i < quads.length; i++) {
    Array_get(&quads, i, (void**) &quad);
    face->face[0][0] = quad->tl.x;
    face->face[0][1] = quad->tl.y;
    face->face[0][2] = quad->tl.z;

    face->face[1][0] = quad->br.x;
    face->face[1][1] = quad->br.y;
    face->face[1][2] = quad->br.z;

    face->face[2][0] = quad->tr.x;
    face->face[2][1] = quad->tr.y;
    face->face[2][2] = quad->tr.z;
    face->next = malloc(sizeof(Face));
    face = face->next;

    face->face[0][0] = quad->tl.x;
    face->face[0][1] = quad->tl.y;
    face->face[0][2] = quad->tl.z;

    face->face[1][0] = quad->bl.x;
    face->face[1][1] = quad->bl.y;
    face->face[1][2] = quad->bl.z;

    face->face[2][0] = quad->br.x;
    face->face[2][1] = quad->br.y;
    face->face[2][2] = quad->br.z;
    face->next = malloc(sizeof(Face));
    face = face->next;
    face->next = NULL;
  }

  Face *prev = face;
  face = mesh->head;
  while (face->next != NULL) {
    mesh->number_of_faces++;
    prev = face;
    face = face->next;
  }
  SOIL_free_image_data(hmap);
  free(face);
  prev->next = NULL;
}
