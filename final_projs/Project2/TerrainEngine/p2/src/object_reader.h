#ifndef OBJECT_READER_H
#define OBJECT_READER_H

#include <glad/glad.h>
#include <cglm/cglm.h>

struct Mesh;
struct Face;

void parse_obj(GLchar* obj_file_path, struct Mesh* out);
void Mesh_free(struct Mesh* mesh);

struct Mesh {
  int number_of_faces;
  struct Face* head;
  void (*free)(struct Mesh*);
} typedef Mesh;

/* NOTE: for now only implemented for triangle meshes */
struct Face {
  vec3 face[3]; /* NOTE should be in order as read from file */
  struct Face* next;
} typedef Face;

#endif /* OBJECT_READER_H */
