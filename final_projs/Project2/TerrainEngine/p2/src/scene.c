#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <string.h>
#include "scene.h"
#include "input.h"
#include "island.h"
#include "skymap.h"
#include "water.h"
#include "array.h"
#include "object.h"

// HACK: should be placed in better place
#define WIDTH 1280
#define HEIGHT 800

// NOTE for now hardcoded should change
void init_Scene(Scene* self) {
  // NOTE could make separate functions
  self->state.fps = 0;
  init_InputState(&(self->state.input_state));
  init_Camera(&self->camera);

  {
    glGenTextures(1, &self->reflection_cubemap);
    glBindTexture(GL_TEXTURE_CUBE_MAP, self->reflection_cubemap);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    for (int i = 0; i < 6; i++) {
      glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_RGB, 512, 512,
                   0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
      glGenerateMipmap(GL_TEXTURE_CUBE_MAP);
    }

    glGenFramebuffers(1, &self->reflection_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, self->reflection_fbo);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_CUBE_MAP_POSITIVE_X,
                           self->reflection_cubemap, 0);

    GLenum draw_buffers[1] = {GL_COLOR_ATTACHMENT0};
    glDrawBuffers(1, draw_buffers);

    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE)
      printf("Framebuffer errror\n");

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
  }

  {
    Shader* our_shader;
    new_Shader(&our_shader, "res/skymap.vert.glsl", "res/skymap.frag.glsl", NULL);

    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    SkyMapArgs args = {
                       .back="res/SkyBox/SkyBox1.bmp",
                       .front="res/SkyBox/SkyBox3.bmp",
                       .left="res/SkyBox/SkyBox0.bmp",
                       .right="res/SkyBox/SkyBox2.bmp",
                       .top="res/SkyBox/SkyBox4.bmp",
                       .shader=our_conf_shader,
    };

    init_SkyMap(&self->skymap, args);
  }

  {
    Shader* our_shader;
    new_Shader(&our_shader, "res/island.vert.glsl", "res/island.frag.glsl", NULL);


    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    IslandArgs args = {
                       .obj_file_path="res/plane.obj",
                       .heightmap="res/heightmap.bmp",
                       .texture1="res/terrain-texture3.bmp",
                       .texture2="res/...",
                       .shader=our_conf_shader,
    };

    init_Island(&self->island, args);
  }

  {
    SkyMap* sky = (SkyMap*) self->skymap.object;
    GLuint sky_cube = sky->cubemap;

    Shader* our_shader;
    new_Shader(&our_shader, "res/water.vert.glsl",
               "res/water.frag.glsl",
               "res/water.geo.glsl");


    ConfigurableShader *our_conf_shader;
    new_ConfigurableShader(&our_conf_shader, our_shader);

    GLfloat offset[] = {-5.0f, 0.14f, -5.0f};
    WaterArgs args = {
                       .obj_file_path="res/plane.obj",
                       .texture1="res/water.bmp",
                       .shader=our_conf_shader,
                       .scale=10.0f,
                       .offset=offset,
                       //.cubemap=self->reflection_cubemap
                       .cubemap=sky_cube
    };

    init_Water(&self->water, args);
  }

  self->render = Scene_render;
  self->update = Scene_update;
}

void new_Scene(Scene** self) {
  (*self) = malloc(sizeof(Scene));
  init_Scene(*self);
}

#define UPDATE_CASE(STATE,KEY)                              \
  if (STATE.KEY == GLFW_PRESS || STATE.KEY == GLFW_REPEAT)
#define STEP_SIZE .1
void Scene_update(Scene* self, SceneState state) {
  self->state = state;
  InputState is = self->state.input_state;

  self->island.update(&self->island, &state);
  self->water.update(&self->water, &state);
  self->skymap.update(&self->skymap, &state);
  Camera_update(&self->camera, state);
}

void Scene_render(Scene* self) {
  /*
  Camera reflect_cam[6];
  for (int i = 0; i < 6; i++) {
    glm_perspective(glm_rad(90.0f), (GLfloat) WIDTH / (GLfloat) HEIGHT,
                    0.1f, 100.0f, reflect_cam[i].projection);
    glm_mat4_identity(reflect_cam[i].view);
    glm_translate(reflect_cam[i].view, (vec3){0.0,0.7,0.0});
    //glm_mat4_copy(self->camera.view, reflect_cam[i].view);
  }

  glm_rotate(reflect_cam[0].view, glm_rad(90), (vec3){0.0, 1.0, 0.0});
  glm_rotate(reflect_cam[1].view, glm_rad(-90), (vec3){0.0, 1.0, 0.0});
  glm_rotate(reflect_cam[2].view, glm_rad(90), (vec3){1.0, 0.0, 0.0});
  glm_rotate(reflect_cam[3].view, glm_rad(-90), (vec3){1.0, 0.0, 0.0});
  glm_rotate(reflect_cam[5].view, glm_rad(180), (vec3){0.0, 1.0, 0.0});

  for (int i = 0; i < 6; i++)
    glm_translate(reflect_cam[i].view, (vec3){0.0, -1.0, 0.0});

  glBindFramebuffer(GL_FRAMEBUFFER, self->reflection_fbo);
  glViewport(0,0,WIDTH,HEIGHT);
  for (int i = 0; i < 6; i++) {
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
                           GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                           self->reflection_cubemap, 0);

    self->island.render(&self->island, &reflect_cam[i]);
    self->skymap.render(&self->skymap, &reflect_cam[i]);
  }
  glBindFramebuffer(GL_FRAMEBUFFER, 0);
  */

  glDisable(GL_DEPTH_TEST);
  self->skymap.render(&self->skymap, &self->camera);
  self->water.render(&self->water, &self->camera);
  self->island.render(&self->island, &self->camera);
  glEnable(GL_DEPTH_TEST);
  self->island.render(&self->island, &self->camera);
  self->skymap.render(&self->skymap, &self->camera);
  self->water.render(&self->water, &self->camera);
}
