#ifndef SKYMAP_H
#define SKYMAP_H

#include <glad/glad.h>

#include "config_shader.h"
#include "scene.h"
#include "object.h"
#include "camera.h"

struct SkyMap;
struct SkyMapArgs;

void init_SkyMap(struct Object*,
                 struct SkyMapArgs);
void new_SkyMap(struct Object**,
                struct SkyMapArgs);
void SkyMap_render(struct Object*, Camera*);
void SkyMap_free(struct Object*);
void SkyMap_update(Object*, SceneState*);

struct SkyMapArgs {
  GLchar* obj_file_path;
  GLchar* front;
  GLchar* left;
  GLchar* right;
  GLchar* back;
  GLchar* top;
  ConfigurableShader* shader;
} typedef SkyMapArgs;

struct SkyMap {
  /* NOTE: vao must assume some position
     arguments about shader -- I don't like
     this but for now this is the case */
  GLuint vao;
  GLuint vbo;
  GLuint number_of_faces;
  GLuint cubemap;
  bool render_step;
  ConfigurableShader* shader;
  unsigned int current_frame;
} typedef SkyMap;

#endif /* SKYMAP_H */
